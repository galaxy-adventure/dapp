export default function getHeroClass(types) {
  const type = Number(types);
  switch (type) {
    case 0:
      return "Navy Seal";
    case 1:
      return "Mercenary";
    case 2:
      return "Assassin";
    default:
      return "---";
  }
};