export const approve = async (
    contract,
    method = {name: "approve", parameters: [string]},
    address
) => {
    try {
        const { name, parameters } = method;
        return await contract.methods[name](...parameters).send({ from: address, gasLimit: "70000" });
    } catch (err) {
        throw { status: 400, name: "Contract error", message: err.message, stack: err.stack };
    }
};
