export const RARE_MAX = 6;
export const UPGRADE_PERCENTAGE = [41.94, 25.81, 16.13, 9.68, 6.45];
export const EXP_THREESHOLD = [990, 3490, 9990, 19990, 39990];
export const RARE_RATIO = 100; // Use to calculate the amount of needed gems
export const timeCoolDowns = [
    6 * 3600,
    5.5 * 3600,
    5 * 3600,
    4.5 * 3600,
    4 * 3600,
    3.5 * 3600,
];