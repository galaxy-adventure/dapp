import Image from 'next/image'

const cloudflareImageLoader = ({ src, width, quality }) => {
	if (!quality) {
		quality = 75
	}

	// If exist cloudflare worker url, then return optimization link
	// else just return normal link
	if (process.env.IMAGES_LOADER && process.env.IMAGES_SOURCE) {
		return `${process.env.IMAGES_LOADER}/?width=${width}&quality=${quality}&image=${process.env.IMAGES_SOURCE}${src}`
	}
	return src
}

export default function Img(props) {
	if (process.env.NODE_ENV === 'development') {
		return <Image unoptimized={true} {...props} />
	} else {
		return <Image {...props} loader={cloudflareImageLoader} />
	}
}