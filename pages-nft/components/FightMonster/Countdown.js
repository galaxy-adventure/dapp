import React, { memo, useState, useEffect, useCallback } from "react";

const Countdown = ({ endTime, setFightBtn }) => {
  const calculateTimeLeft = useCallback(() => {
    const difference = new Date(endTime) - new Date();
    let timeLeft = {};

    if (difference > 0) {
      timeLeft = {
        days: Math.floor(difference / (1000 * 60 * 60 * 24)),
        hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
        minutes: Math.floor((difference / 1000 / 60) % 60),
        seconds: Math.floor((difference / 1000) % 60),
      };
    }
    if (
      !timeLeft.days &&
      !timeLeft.hours &&
      !timeLeft.minutes &&
      !timeLeft.seconds
    ) {
      return null;
    }
    return timeLeft;
  }, [endTime]);

  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());
  useEffect(() => {
    const timerTimeOut = setTimeout(() => {
      setTimeLeft(calculateTimeLeft());
    }, 1000);

    return () => clearTimeout(timerTimeOut);
  });

  useEffect(() => {
    if (!timeLeft) {
      setFightBtn(true);
    }
  }, [timeLeft, setFightBtn])

  const timerComponents = [];

  timeLeft &&
    Object.keys(timeLeft).forEach((interval, idx) => {
      if (!timeLeft[interval]) {
        return;
      }
      timerComponents.push(
        <small key={idx}>
          {timeLeft[interval]}
          {interval.substr(0, 1)}{" "}
        </small>
      );
    });

  return (
    <p>
      {timerComponents && timerComponents.length > 0
        ? timerComponents
        : "00:00:00"}
    </p>
  );
};

export default memo(Countdown);
