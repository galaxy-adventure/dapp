import Image from "../Image";
import React, { useState, useContext, useRef } from "react";
import { Col, Modal } from "react-bootstrap";
import { useMoralis } from "react-moralis";
import Swal from "sweetalert2";
import SoundContext from "../../context/SoundContext";
import { BATTLE_ABI, BATTLE_CONTRACT } from "../../utils/const";
import { EXP_THREESHOLD } from "../../utils/HeroData";

export default function ItemFightMonster({
  data,
  heroData,
  remainTime,
  reloadHero,
}) {
  const { web3 } = useMoralis();
  const [show, setShow] = useState(false);
  const { soundStatus, setSoundStatus } = useContext(SoundContext);
  const pauseSound = useRef(false);

  const handleClose = () => {
    setShow(false);
    if (pauseSound.current) {
      setSoundStatus(true);
      pauseSound.current = false;
    }
  };
  const handleBattle = () => {
    if (soundStatus) {
      setSoundStatus(false);
      pauseSound.current = true;
    }
    handleBattleResult();
    setShow(true);
  };

  const handleBattleResult = () => {
    const battleContract = new web3.eth.Contract(BATTLE_ABI, BATTLE_CONTRACT);
    loadBattle(battleContract);
  };
  const loadBattle = (battleContract) => {
    try {
      /* battleContract.events.BattleResult(
        {
          filter: { heroId: heroData.id },
          fromBlock: 0,
        },
        (error, event) => {
          if (event) {
            console.log(event);
            heroData.exp = parseInt(heroData.exp) + parseInt(event.returnValues.expReward);
            heroData.lastBattleTime = parseInt(Date.now()/1000);
            reloadHero();
          }
        }
      ); */
      battleContract.once(
        "BattleResult",
        {
          filter: { heroId: heroData.id },
          fromBlock: 'lastest',
        },
        (error, event) => {
          if (event) {
            heroData.exp = parseInt(heroData.exp) + parseInt(event.returnValues.expReward);
            heroData.lastBattleTime = parseInt(Date.now() / 1000);
            (heroData.exp > EXP_THREESHOLD[heroData.level - 1]) ? heroData.level += 1 : null;
            reloadHero();
          }
        }
      );
    } catch (err) {
      // console.log(err);
      Swal.fire("Transaction error!", "", "error");
    } finally {
    }
  };

  return (
    <Col sm={12} md={6} lg={6} xl={3} className="text-center">
      <div className="card-monster card">
        <div className="card-monster__img">
          <Image
            src={data.image}
            alt="card-monster"
            className="img-card-monster"
            width={240}
            height={210}
          />
        </div>
        <div className="card-body">
          <h3 className="card-title">{data.name}</h3>
          <div className="monster-statis">
            <div className="moster-statis-row">
              <span className="monster-statis__title">Level</span>
              <span className="monster-statis__value">{data.level}</span>
            </div>
            <div className="moster-statis-row">
              <span className="monster-statis__title">Approx Win Rate</span>
              <span className="monster-statis__value">~{data.winRate}%</span>
            </div>
            <div className="moster-statis-row">
              <span className="monster-statis__title">Estimated Reward</span>
              <span className="monster-statis__value">
                {data.rewardEstimated}
              </span>
            </div>
            <div className="moster-statis-row">
              <span className="monster-statis__title">Estimated EXP</span>
              <span className="monster-statis__value">{data.expEstimated}</span>
            </div>
          </div>
          <div className="mt-3">
            {data.available == true ? (
              Number(remainTime) === 0 ? (
                <button
                  className="btn btn-warning btn-block"
                  onClick={handleBattle}
                >
                  Fight
                </button>
              ) : (
                <button className="btn btn-warning btn-block">
                  <i className="fa fa-redo-alt fa-spin"></i>
                </button>
              )
            ) : (
              <button className="btn btn-warning btn-block">Coming soon</button>
            )}
          </div>
        </div>
      </div>

      <Modal
        show={show}
        size="lg"
        className="modal-fighting"
        backdrop="static"
        keyboard={false}
        centered
        onHide={handleClose}
      >
        <Modal.Header closeButton>
          <Modal.Title>BATTLE</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="embed-responsive embed-responsive-16by9">
            <iframe
              width="100%"
              height="100%"
              className="embed-responsive-item"
              src={`/gameplay/index.html?h=${heroData.types}&m=${data.level}&heroid=${heroData.id}&s=${heroData.rare}&l=${heroData.level}`}
            ></iframe>
          </div>
        </Modal.Body>
      </Modal>
    </Col>
  );
}
