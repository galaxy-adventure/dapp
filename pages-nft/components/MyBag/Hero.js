import { useRouter } from "next/dist/client/router";
import Image from "../Image";
import Link from "next/link";
import React, { useState, useEffect } from "react";
import { Col, Form, ProgressBar } from "react-bootstrap";
import { useMoralis } from "react-moralis";
import Swal from "sweetalert2";
import Modal from "../../components/Modal/Modal";
import getHeroClass from "../../utils/app-util";
import {
  ITEM_ABI,
  ITEM_CONTRACT,
  MARKET_ABI,
  MARKET_CONTRACT,
  UPGRADE_ABI,
  UPGRADE_CONTRACT,
  HERO_ABI,
  HERO_CONTRACT,
} from "../../utils/const";
import setLoading from "../../utils/loading";
import { truncate } from "../../utils/string";
import { RARE_MAX, UPGRADE_PERCENTAGE, EXP_THREESHOLD, RARE_RATIO } from "../../utils/HeroData";

export default function Hero({ data, changeHeroData }) {
  const gemNeeded = data.rare * RARE_RATIO;
  const truncatedName = truncate(data.name, 12);

  const router = useRouter();
  const { user, web3 } = useMoralis();
  const [showUpgrade, setShowUpgrade] = useState(false);
  const [approveSell, setApproveSell] = useState(false);
  const [amountGem, setAmountGem] = useState(0);
  const [amountCore, setAmountCore] = useState(0);
  const [selectCore, setSelectCore] = useState(false);
  const [sellHeroData, setSellHeroData] = useState({
    id: 1,
    types: 0,
    name: "Jax",
    rare: 1,
    level: 1,
    price: 0,
  });
  const [sellPrice, setSellPrice] = useState(0);
  const handleChangePrice = (e, type) => {
    let value = e.target.value;
    setSellPrice(value);
  };

  const handleSelectRare = (types, rare) => {
    return `/assets/img/dapp/heros/${types}/R${rare}.gif`;
  };
  const createStarImage = (onOff, index) => (
    <Image
      key={`star-${index}`}
      id={`star-${index}`}
      src={`/assets/images/stones/star${onOff}.png`}
      width={32}
      height={32}
      alt="Star"
    />
  );
  const showStars = (rare) => {
    let stars = [];
    for (var i = 0; i < rare; i++) {
      stars.push(1);
    }
    for (var i = rare; i < 6; i++) {
      stars.push(2);
    }
    return stars.map(createStarImage);
  };
  const handleListSell = (heroData) => {
    setSellHeroData({
      id: heroData.id,
      types: heroData.types,
      name: heroData.name,
      rare: heroData.rare,
      level: heroData.level,
      price: heroData.price,
    });
    setSellPrice(heroData.price);
    setApproveSell(true);
  };
  const handleApprove = async (heroId, price) => {
    setLoading(true);
    try {
      const marketContract = new web3.eth.Contract(MARKET_ABI, MARKET_CONTRACT);
      await marketContract.methods
        .listOnMarket(heroId, price)
        .send({ from: user.get("ethAddress") })
        .then(() => {
          setLoading(false);
          Swal.fire("List sell successfully!", "", "success");
          setApproveSell(false);
          // Reload mybag
          changeHeroData(1);
        });
    } catch (err) {
      setLoading(false);
      Swal.fire("List sell failed!", "", "error");
    }
  };
  const loadHerpItems = async () => {
    setLoading(true);
    try {
      // Get GEM
      const itemContract = new web3.eth.Contract(ITEM_ABI, ITEM_CONTRACT);
      await itemContract.methods
        .balanceOf(user.get("ethAddress"), 1)
        .call()
        .then((res) => {
          setAmountGem(parseInt(res));
        });
      // Get CORE
      await itemContract.methods
        .balanceOf(user.get("ethAddress"), 2)
        .call()
        .then((res) => {
          setAmountCore(parseInt(res));
        });
    } catch (err) {
      // console.log(err);
    } finally {
      setLoading(false);
      setShowUpgrade(true);
    }
  };

  const handleChangeName = async (heroID, currentName) => {
    const { value: newName } = await Swal.fire({
      title: "Enter your hero's name",
      input: "text",
      inputLabel: "Hero's name",
      inputValue: currentName,
      showCancelButton: true,
      inputValidator: (value) => {
        if (!value) {
          return "You need to write a name!";
        } else if (value === currentName) {
          return "You need to set a different name!";
        }
      },
    });
    if (newName) {
      setLoading(true);
      try {
        const heroContract = new web3.eth.Contract(HERO_ABI, HERO_CONTRACT);
        await heroContract.methods
          .setHeroName(heroID, newName)
          .send({ from: user.get("ethAddress") })
          .then(() => {
            setLoading(false);
            Swal.fire("Change hero's name successfully!", "", "success");
            // Reload mybag
            changeHeroData(Math.random());
          });
      } catch (err) {
        setLoading(false);
        Swal.fire("Change hero's name failed!", "", "error");
      }
    }
  };

  const handleSelectCore = () => {
    if (amountCore > 0) {
      setSelectCore(!selectCore);
    } else {
      router.push({
        pathname: "/",
        query: { defaultKey: "item" },
      });
    }
  };

  const checkUpgradeCriteria = () => {
    if (data.rare >= RARE_MAX) {
      Swal.fire("Your hero reached the maximum level", "", "warning");
    } else if (amountGem < gemNeeded) {
      Swal.fire("You don't have enough gems", "", "warning");
    } else if (data.rare === 1 && !selectCore) {
      Swal.fire("Your hero need a core to upgrade", "", "warning");
    } else {
      warningAndUpgrade();
    }
  };

  const warningAndUpgrade = () => {
    if (selectCore) {
      handleUpgradeHero(data.id);
    } else {
      Swal.fire({
        title: "Are you sure to upgrade without core?",
        html: "<span style='color:white;font-size:20'>Your hero's rare may be downgraded</span>",
        icon: "warning",
        showCancelButton: true,
        cancelButtonColor: "#d33",
        confirmButtonColor: "#33cc33",
        confirmButtonText: "Upgrade",
      }).then((result) => {
        if (result.isConfirmed) {
          handleUpgradeHero(data.id);
        }
      });
    }
  };

  const handleUpgrade = (heroId) => {
    Swal.fire({
      title: "WARNING & DISCLAIMER",
      html: "<span style='color:white;'>Upgrading your hero is based on a probability system, and is not 100% guaranteed!! It's a high risk / high reward scenario!! For further explanation please read our white paper!!<br>Materials used during an upgrade will be lost!!<br>Please create a strategy for upgrading, and upgrade at your own risk!!<br>Please be aware that there is a fee associated with upgrading your hero.</span>",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Understood, I want to upgrade",
    }).then((result) => {
      if (result.isConfirmed) {
        loadHerpItems();
      }
    });
  };

  const handleUpgradeHero = async (heroId) => {
    setLoading(true);
    try {
      const upgradeContract = new web3.eth.Contract(
        UPGRADE_ABI,
        UPGRADE_CONTRACT
      );

      upgradeContract.once(
        "Upgrade",
        {
          filter: { heroId: heroId },
          fromBlock: "lastest",
        },
        (error, event) => {
          // console.log(event);
          if (event.returnValues.status === true) {
            Swal.fire("Upgrade Successful!", "", "success");
          } else {
            Swal.fire("Upgrade NOT Successful!", "", "error");
          }
          // Reload bag
          changeHeroData(Math.random());
          setShowUpgrade(false);
          setLoading(false);
        }
      );
      await upgradeContract.methods
        .upgradeHero(heroId, selectCore)
        .send({ from: user.get("ethAddress") })
        .then((res) => {
          // setLoading(false);
        });
    } catch (err) {
      // console.log(err);
      setLoading(false);
      setShowUpgrade(false);
      Swal.fire("Upgrade failed!", "", "error");
    }
  };
  const handleWithdraw = async (heroId) => {
    Swal.fire({
      title: "Are you sure?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Ok",
      denyButtonText: `Cancel`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        setLoading(true);
        try {
          const marketContract = new web3.eth.Contract(
            MARKET_ABI,
            MARKET_CONTRACT
          );
          await marketContract.methods
            .delistAndWithdraw(heroId)
            .send({ from: user.get("ethAddress") })
            .then(() => {
              setLoading(false);
              // Reload mybag
              changeHeroData(Math.random());
            });
        } catch (err) {
          setLoading(false);
          Swal.fire("Withdraw failed!", "", "error");
        }
      }
    });
  };

  // Select core if users have core
  useEffect(() => {
    if (amountCore > 0) {
      setSelectCore(true);
    } else {
      setSelectCore(false);
    }
  }, [amountCore]);

  return (
    <>
      <Col key={data.id} md={6} lg={3}>
        <div className="card-shop">
          <div className="card-shop-ava">
            <div className="card-shop-bg">
              <Image
                key={`frame-${data.id}`}
                id={`frame-${data.id}`}
                src="/assets/images/pages/store/base.png"
                width={372}
                height={142}
                alt="base"
              />
            </div>
            <div className="card-shop-anim">
              <div className="card-shop-img">
                <Image
                  key={`hero-${data.id}`}
                  id={`hero-${data.id}`}
                  src={handleSelectRare(data.types, data.rare)}
                  width={400}
                  height={400}
                  alt="Hero"
                />
              </div>
            </div>
          </div>
          <div className="card-shop-meta">
            <div className="header-card">
              <button
                className="edit-btn"
                onClick={() => handleChangeName(data.id, data.name)}
              ></button>
              <div className="card-shop-name">{truncatedName}</div>
              <div className="index-number">#{data.id}</div>
            </div>
            <div className="card-shop-infor">
              <div className="card-shop-prices">
                <div className="card-shop-row">{showStars(data.rare)}</div>
                <div className="card-shop-row">
                  <div className="card-shop-label">Level</div>
                  <div className="card-shop-value">{data.level}</div>
                </div>
                <div className="card-shop-row">
                  <div className="card-shop-label">Experience</div>
                  <div className="card-shop-value">
                    {data.exp}
                    {data.level && data.level < RARE_MAX
                      ? ` / ${EXP_THREESHOLD[data.level - 1]}`
                      : null}
                  </div>
                </div>
                <ProgressBar
                  className="exp-bar"
                  animated
                  now={
                    data.level
                      ? [
                        data.level < RARE_MAX
                          ? (data.exp / EXP_THREESHOLD[data.level - 1]) * 100
                          : 100,
                      ]
                      : 0
                  }
                />
                <div className="card-shop-row">
                  <div className="card-shop-label">Class</div>
                  <div className="card-shop-value">
                    {getHeroClass(data.types)}
                  </div>
                </div>
                {/* {data.listMarket ? (
                <div className="card-shop-row">
                  <div className="card-shop-label">Price</div>
                  <div className="card-shop-value">
                    <Image
                      id={`gla-coin-${data.id}`}
                      src="/assets/images/logo/gla-coin.png"
                      width={16}
                      height={16}
                      alt="GLA Coin"
                    />
                    &nbsp;{data.price}
                  </div>
                </div>
                ) : (
                  <span>&nbsp;</span>
                )} */}
              </div>
            </div>
            {data.listMarket ? (
              <div className="card-shop-action">
                <button
                  className="card-shop-button d-block button-connect"
                  onClick={() => handleWithdraw(data.id)}
                >
                  Withdraw
                </button>
                <span className="card-shop-button d-block">&nbsp;</span>
              </div>
            ) : (
              <div className="card-shop-action">
                <button
                  className="card-shop-button d-block button-connect"
                  onClick={() => handleListSell(data)}
                >
                  List Sell
                </button>
                <button
                  className="card-shop-button d-block button-buy"
                  onClick={() => handleUpgrade()}
                >
                  Upgrade
                </button>
              </div>
            )}
          </div>
        </div>
      </Col>
      <Modal show={showUpgrade}>
        <div className="modal-upgrade">
          <div className="modal-items">
            <Link
              href={{
                pathname: "/",
                query: { defaultKey: "item" },
              }}
              passHref
            >
              <button className="modal__items--gem">
                <Image
                  src={`/assets/images/pages/my-bag/gem.png`}
                  width={100}
                  height={100}
                  alt="Gem"
                />
                <span
                  className="modal__items--gem-text"
                  style={
                    amountGem < gemNeeded
                      ? { color: "red", fontWeight: "bold" }
                      : null
                  }
                >
                  {gemNeeded}/{amountGem}
                </span>
              </button>
            </Link>
            <Image
              key={`plus-hero-${data.id}`}
              id={`plus-hero-${data.id}`}
              src={`/assets/images/pages/my-bag/cong.png`}
              width={50}
              height={50}
              alt="Plus"
            />
            <button
              className="modal__items--core modal__items--gem"
              onClick={() => handleSelectCore()}
            >
              <Image
                key={`plus-core-${data.id}`}
                id={`plus-core-${data.id}`}
                src={`/assets/images/pages/my-bag/core.png`}
                width={100}
                height={100}
                alt="Core"
              />
              <div className="checkbox">
                {selectCore ? (
                  <Image
                    key={`plus-icon-check-${data.id}`}
                    id={`plus-icon-check-${data.id}`}
                    src="/assets/images/pages/marketplace/icon-check.png"
                    alt="check"
                    width="25"
                    height="25"
                  />
                ) : (
                  <Image
                    key={`plus-icon-uncheck-${data.id}`}
                    id={`plus-icon-uncheck-${data.id}`}
                    src="/assets/images/pages/marketplace/icon-uncheck.png"
                    alt="checkbox-background"
                    width="25"
                    height="25"
                  />
                )}
              </div>
              <span
                className="modal__items--gem-text"
                style={
                  amountCore === 0 ? { color: "red", fontWeight: "bold" } : null
                }
              >
                1/{amountCore}
              </span>
            </button>
          </div>
          <div className="modal__upgrade--progress">
            <div className="modal__hero">
              <div className="card-shop">
                <div className="card-shop-ava">
                  <div className="card-shop-bg card-shop-base-from">
                    <Image
                      key={`upgrade-frame1-${data.id}`}
                      id={`upgrade-frame1-${data.id}`}
                      src="/assets/images/pages/store/base.png"
                      width={372}
                      height={142}
                      alt="Base"
                    />
                  </div>
                  <div className="card-shop-anim-from">
                    <div className="card-shop-img-vertical">
                      <Image
                        className="card-shop-hero"
                        key={`upgrade-from-hero-${data.id}`}
                        id={`upgrade-from-hero-${data.id}`}
                        src={handleSelectRare(data.types, data.rare)}
                        width={200}
                        height={200}
                        alt="Upgrade from hero"
                      />
                    </div>
                  </div>
                </div>
                <div className="card-shop-infor">
                  <div className="card-shop-prices">
                    <div className="card-shop-row">{showStars(data.rare)}</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="image__arrow">
              <Image
                key={`upgrade-arrow-${data.id}`}
                id={`upgrade-arrow-${data.id}`}
                src={`/assets/images/pages/my-bag/arrow.png`}
                width={100}
                height={100}
                alt="Star"
              />
            </div>
            <div className="image__arrow--down">
              <Image
                key={`upgrade-arrow-down-${data.id}`}
                id={`upgrade-arrow-down-${data.id}`}
                src={`/assets/images/pages/my-bag/arrow_down.png`}
                width={100}
                height={100}
                alt="Star"
              />
            </div>
            {data.rare < RARE_MAX && (
              <h5 className="percentage">
                UP TO {UPGRADE_PERCENTAGE[data.rare - 1]}% probability for a
                SUCCESSFUL UPGRADE
              </h5>
            )}
            <div className="modal__hero--upgrade">
              <div className="card-shop">
                <div className="card-shop-ava">
                  <div className="card-shop-bg card-shop-base-to">
                    <Image
                      key={`upgrade-frame2-${data.id}`}
                      id={`upgrade-frame2-${data.id}`}
                      src="/assets/images/pages/store/base.png"
                      width={372}
                      height={142}
                      alt="Base"
                    />
                  </div>
                  <div className="card-shop-anim-to">
                    <div className="card-shop-img-vertical">
                      {data.rare < RARE_MAX ? (
                        <Image
                          className="card-shop-hero"
                          key={`upgrade-to-hero-${data.id}`}
                          id={`upgrade-to-hero-${data.id}`}
                          src={handleSelectRare(data.types, data.rare + 1)}
                          width={200}
                          height={200}
                          alt="Upgrade to hero"
                        />
                      ) : (
                        <span className="maximum-text">
                          Cannot Upgrade 6-STAR Hero
                        </span>
                      )}
                    </div>
                  </div>
                </div>
                <div className="card-shop-infor">
                  <div className="card-shop-prices">
                    <div className="card-shop-row">
                      {data.rare < RARE_MAX
                        ? showStars(data.rare + 1)
                        : showStars(data.rare)}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="modal__button--group">
          <button
            className="card-shop-button d-block button-connect"
            onClick={() => setShowUpgrade(false)}
          >
            Close
          </button>
          <button
            className="card-shop-button d-block button-buy"
            onClick={() => checkUpgradeCriteria()}
          // disabled={!checkUpgradeCriteria()}
          >
            Upgrade
          </button>
        </div>
      </Modal>

      {/* Show hero to approve */}
      <Modal show={approveSell}>
        <div className="modal-upgrade">
          <div className="modal__upgrade--progress">
            <div className="modal__hero--upgrade">
              <div className="card-shop">
                <div className="card-shop-ava">
                  <div className="card-shop-bg card-shop-bg-list-market">
                    <Image
                      key={`upgrade-frame2-${sellHeroData.id}`}
                      id={`upgrade-frame2-${sellHeroData.id}`}
                      src="/assets/images/pages/store/base.png"
                      width={372}
                      height={142}
                      alt="Base"
                    />
                  </div>
                  <div className="card-shop-anim card-shop-anim-listmarket">
                    <div className="card-shop-img">
                      <Image
                        key={`sell-hero-${sellHeroData.id}`}
                        id={`sell-hero-${sellHeroData.id}`}
                        src={handleSelectRare(
                          sellHeroData.types,
                          sellHeroData.rare
                        )}
                        width={200}
                        height={200}
                        alt="Sell hero"
                      />
                    </div>
                  </div>
                </div>
                <div className="card-shop-infor">
                  <div className="card-shop-prices">
                    <div className="card-shop-row">
                      {showStars(sellHeroData.rare)}
                    </div>
                  </div>
                </div>
                <div className="card-shop-infor">
                  <div className="card-shop-prices">
                    <div className="card-shop-row">
                      <Image
                        key={`gla-coin-${data.id}`}
                        id={`gla-coin-${data.id}`}
                        src="/assets/images/logo/gla-coin.png"
                        width={32}
                        height={32}
                        alt="GLA Coin"
                      />
                      &nbsp;
                      <Form.Control
                        size="sm"
                        type="number"
                        minLength={3}
                        maxLength={18}
                        value={sellPrice}
                        onChange={(value) =>
                          handleChangePrice(value, sellHeroData.types)
                        }
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="modal__button--group">
          <button
            className="card-shop-button d-block button-connect"
            onClick={() => setApproveSell(false)}
          >
            Close
          </button>
          <button
            className="card-shop-button d-block button-buy"
            onClick={() => handleApprove(sellHeroData.id, sellPrice)}
          >
            Approve
          </button>
        </div>
      </Modal>
    </>
  );
}
