import Image from "../Image";
import Link from "next/link";
import React, { useState, useEffect } from "react";
import { Col } from "react-bootstrap";
import { useMoralis } from "react-moralis";
import Swal from "sweetalert2";
import Modal from "../../components/Modal/Modal";
import { SPAWN_ABI, SPAWN_CONTRACT } from "../../utils/const";
import setLoading from "../../utils/loading";
import { useRouter } from "next/router";

export default function Item({ data, changeHeroData }) {
  const { user, web3 } = useMoralis();
  const router = useRouter();
  const [showOpenSuccess, setShowOpenSuccess] = useState(false);
  const [welcomeMessage, setWelcomeMessage] = useState();
  const [openedHero, setOpenedHero] = useState({
    id: 0,
    types: 1,
    name: "",
    rare: 1,
    level: 0,
    exp: 0,
  });
  const handleSelectRare = (types, rare) => {
    return `/assets/img/dapp/heros/${types}/R${rare}.gif`;
  };
  const getHeroName = (types) => {
    switch (types) {
      case 0:
        return "Ray";
      case 1:
        return "Jax";
      case 2:
        return "Kat";
      default:
        return "---";
    }
  };
  const createStarImage = (onOff, index) => (
    <Image
      key={`star-${index}`}
      id={`star-${index}`}
      src={`/assets/images/stones/star${onOff}.png`}
      width={32}
      height={32}
      alt="Star"
    />
  );
  const showStars = (rare) => {
    let stars = [];
    for (var i = 0; i < rare; i++) {
      stars.push(1);
    }
    for (var i = rare; i < 6; i++) {
      stars.push(2);
    }
    return stars.map(createStarImage);
  };
  const handleOpenChest = async (heroId) => {
    setLoading(true);
    try {
      const chestContract = new web3.eth.Contract(SPAWN_ABI, SPAWN_CONTRACT);
      await chestContract.methods
        .openERC1155Chest()
        .send({ from: user.get("ethAddress") })
        .then((res) => {
          const retVal = res.events.OpenChest.returnValues;
          setOpenedHero({
            id: 0,
            types: parseInt(retVal.heroType),
            name: "",
            rare: parseInt(retVal.rarity),
            level: 1,
            exp: 0,
          });
          setLoading(false);
          setShowOpenSuccess(true);
          // Reload mybag
          changeHeroData(Math.random());
        });
    } catch (err) {
      // console.log(err);
      setLoading(false);
      setShowOpenSuccess(false);
      Swal.fire("Open failed!", "", "error");
    }
  };
  const handleClick = (e) => {
    e.preventDefault();
    router.push("/");
  };

  // Set new hero name when opening from chest
  useEffect(() => {
    const getWelcomeMessage = (types) => {
      const name = getHeroName(types);
      return name + " has joined your squad!";
    };
    setWelcomeMessage(getWelcomeMessage(openedHero.types, openedHero.rare));
  }, [openedHero]);

  return (
    <>
      <Col key={data.id} md={6} lg={3}>
        <div className="card-shop">
          <div className="card-shop-ava">
            <div className="card-shop-bg">
              <Image
                key={`base-item-${data.id}`}
                id={`base-item-${data.id}`}
                src="/assets/images/pages/store/base.png"
                width={372}
                height={142}
                alt="Base"
              />
            </div>
            <div className="card-shop-anim">
              <div className="card-shop-img">
                <Image
                  key={`core-item-${data.id}`}
                  id={`core-item-${data.id}`}
                  src={data.image}
                  width={110}
                  height={160}
                  alt="Core"
                />
              </div>
            </div>
          </div>
          <div className="card-shop-meta">
            <div className="card-shop-name">{data.name}</div>
            <div className="card-shop-infor">
              <div className="card-shop-prices">
                <div className="card-shop-row">
                  <div className="card-shop-label">Amount</div>
                  <div className="card-shop-value">{data.amount}</div>
                </div>
              </div>
            </div>
            {data.types === 3 ? (
              <div className="card-shop-action">
                {data.amount > 0 ? (
                  <button
                    className="card-shop-button d-block button-buy"
                    onClick={() => handleOpenChest(data.id)}
                  >
                    Open
                  </button>
                ) : (
                  <span className="card-shop-button d-block button-buy">
                    <Link
                      href={{
                        pathname: "/",
                        query: { defaultKey: "item" },
                      }}
                    >
                      Go to Store
                    </Link>
                  </span>
                )}
                <p className="card-description">
                  Gives you the chance to win a hero that you may use in the
                  game. The highest rated hero available is 3 STAR.
                </p>
              </div>
            ) : (
              <div>
                <span className="card-shop-button d-block button-buy">
                  <Link
                    href={{
                      pathname: "/",
                      query: { defaultKey: "item" },
                    }}
                  >
                    Go to Store
                  </Link>
                </span>
                {data.types === 1 ? (
                  <p className="card-description">
                    Use GEMS to upgrade your hero and buy weapons for in-game
                    use.
                  </p>
                ) : (
                  <p className="card-description">
                    Use CORE to protect your hero while they upgrade. Upgrading
                    without CORE may cause your hero to LOSE the upgrade.
                  </p>
                )}
              </div>
            )}
          </div>
        </div>
      </Col>

      {/* Show hero open success */}
      <Modal show={showOpenSuccess}>
        <div className="modal-upgrade">
          <div className="modal__upgrade--progress">
            <div className="modal__hero--upgrade">
              <div className="card-shop">
                <div className="card-shop-ava">
                  <div className="card-shop-anim-title">
                    <span>{welcomeMessage}</span>
                  </div>
                  <div className="card-shop-bg">
                    <Image
                      key={`upgrade-frame2-${openedHero.id}`}
                      id={`upgrade-frame2-${openedHero.id}`}
                      src="/assets/images/pages/store/base.png"
                      width={372}
                      height={142}
                      alt="Base"
                    />
                  </div>
                  <div className="card-shop-anim">
                    <div className="card-shop-img">
                      <Image
                        key={`open-hero-${openedHero.id}`}
                        id={`open-hero-${openedHero.id}`}
                        src={handleSelectRare(
                          openedHero.types,
                          openedHero.rare
                        )}
                        width={200}
                        height={200}
                        alt="Open hero"
                      />
                    </div>
                  </div>
                </div>
                <div className="card-shop-infor">
                  <div className="card-shop-prices">
                    <div className="card-shop-row">
                      {showStars(openedHero.rare)}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="modal__button--group">
          <button
            className="card-shop-button d-block button-connect"
            onClick={() => setShowOpenSuccess(false)}
          >
            OK
          </button>
        </div>
      </Modal>
    </>
  );
}
