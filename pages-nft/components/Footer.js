import React from 'react';
import { Container } from 'react-bootstrap';
import Community from './Index/Community';

export default function Footer() {
    return (
        <footer>
            <Container>
                <Community></Community>
            </Container>
            <div className="copyright-area d-flex flex-wrap justify-content-center">
                <div className="copyright-content">
                    <p>
                        Copyright © 2021. All Rights Reserved By&nbsp;
                        <a href="https://galaxyadventure.io/" target="_blank" rel="noreferrer">Galaxy Adventure</a>
                    </p>
                </div>
            </div>
        </footer>
    );
}
