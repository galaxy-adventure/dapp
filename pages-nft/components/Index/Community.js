import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Image from '../Image';
import Link from 'next/link';

export default function Community() {
    return (
        <div className="footer-section" id="community">
            <Container>
                <div className="footer-content">
                    <Row className="align-items-center">
                        <Col lg={4} className="text-center mb-3">

                            <Link href="/">
                                <a className="logo-footer">
                                    <Image
                                        src="/assets/img/logo.png"
                                        width="200"
                                        height="75"
                                    />
                                </a>
                            </Link>
                        </Col>
                        <Col lg={4} className="text-center mb-3">
                            <h3 className="sub-title">GET IN TOUCH</h3>
                            <h4 className="title">Join our community</h4>
                        </Col>
                        <Col lg={4} className="text-center mb-3">
                            <ul className="footer-social-icon">
                                <li>
                                    <a
                                        href="https://t.me/GalaxyAdventureIO"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <img
                                            src="/assets/img/icon_tele.png"
                                            alt="element"
                                        />
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="https://www.reddit.com/user/galaxy_adventure"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <img
                                            src="/assets/img/icon_reddit.png"
                                            alt="element"
                                        />
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="https://twitter.com/galaxynftgame"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <img
                                            src="/assets/img/icon_twitter.png"
                                            alt="element"
                                        />
                                    </a>
                                </li>
                            </ul>
                        </Col>
                    </Row>
                </div>
            </Container>
        </div>
    );
}
