import React, { useEffect, useState } from 'react';
import { Container, Button, OverlayTrigger, Tooltip } from 'react-bootstrap';
import Link from 'next/link';
import { useMoralis } from 'react-moralis';
import { truncate } from '../utils/string';
import Image from './Image';
import { useRouter } from 'next/router';
import Swal from 'sweetalert2';
import { TwitterShareButton, TwitterIcon, FacebookShareButton, FacebookIcon, TelegramShareButton, TelegramIcon } from 'react-share';
import { BATTLE_ABI, BATTLE_CONTRACT, TOKEN_ABI, TOKEN_CONTRACT } from '../utils/const'

export default function Header() {
    const { authenticate, logout, isAuthenticated, user, Moralis, web3, enableWeb3,
        isWeb3Enabled } = useMoralis();
    const [chainStatus, setStatus] = useState(0);
    const [balanceToken, setBalanceToken] = useState(0);
    const [claimableBattleFee, setClaimableBattleFee] = useState(0);

    const ATTENTION = "";
    const shareUrl = 'https://galaxyadventure.io/';
    const title = `I have accumulated ${claimableBattleFee} $GLA from the battle fee.\nLet's join our community now! We will go to the moon together!!! `;

    const handleLogin = async () => {
        await authenticate();
    };
    const handleLogout = async () => {
        await logout();
    };
    const handleClick = () => {
        if (!isAuthenticated) {
            handleLogin();
        }
    };

    const router = useRouter();

    const getBalanceToken = async () => {
        if (isAuthenticated) {
            const tokenContract = new web3.eth.Contract(TOKEN_ABI, TOKEN_CONTRACT);
            await tokenContract.methods
                .balanceOf(user.get("ethAddress"))
                .call()
                .then((res) => {
                    const balance = parseInt(res) / (10 ** 18);
                    setBalanceToken(balance.toFixed(3));
                });
        }
    }

    const getClaimableFee = async () => {
        if (isAuthenticated) {
            const battleContract = new web3.eth.Contract(BATTLE_ABI, BATTLE_CONTRACT);
            await battleContract.methods
                .getClaimableFee(user.get("ethAddress"))
                .call()
                .then((res) => {
                    const balance = parseInt(res) / (10 ** 18);
                    setClaimableBattleFee(balance.toFixed(0));
                });
        }
    }

    const handleClaimClick = async () => {
        if (claimableBattleFee < 1000) {
            Swal.fire("Minimum claimable amount is 1000 GLA", "", "warning");
        } else {
            if (isAuthenticated && claimableBattleFee >= 1000) {
                const battleContract = new web3.eth.Contract(BATTLE_ABI, BATTLE_CONTRACT);
                await battleContract.methods
                    .claimBattleFee()
                    .send({ from: user.get("ethAddress") })
                    .then((res) => {
                        setClaimableBattleFee(0);
                        getBalanceToken();
                    });
            }
        }
    }

    useEffect(() => {
        if (!isWeb3Enabled) {
            enableWeb3();
            if (web3.currentProvider) {
                setStatus(web3.currentProvider.chainId);
            }
        }

    }, [web3.currentProvider]);

    useEffect(() => {
        const timerTimeOut = setTimeout(() => {
            getBalanceToken();
            getClaimableFee();
        }, 10000);

        return () => clearTimeout(timerTimeOut);
    });

    useEffect(async () => {
        // if (chainStatus && chainStatus !== '0x38') {
        //     await handleLogout();
        //     Swal.fire({
        //         title: "Wrong network",
        //         html: '<span style="color: red; font-size: 24">Wrong network</span>',
        //         icon: 'warning',
        //         confirmButtonText: "Close",
        //     });
        // }
        getBalanceToken();
        getClaimableFee();
    }, [chainStatus])

    useEffect(() => {
        Moralis.onAccountsChanged(async function (accounts) {
            await handleLogout();

            Swal.fire({
                title: 'Are you sure you want to login with this account?',
                html: '<span style="color: yellow; font-size: 24">This account will be logged in</span>',
                icon: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, log me in'
            }).then((result) => {
                if (result.isConfirmed) {
                    handleLogin();
                }
            })
        });

        Moralis.onChainChanged(async function (chain) {
            setStatus(chain);
        });

    }, [])

    const getMenuTitle = (pathname) => {
        let title = '';
        switch (pathname) {
            case '/marketplace':
                title = 'Marketplace';
                break;
            case '/fight-monster':
                title = 'BATTLE';
                break;
            case '/mybag':
                title = 'Inventory';
                break;
            case '/':
                title = 'Store';
                break;
            case '/pvp':
                title = 'PvP';
                break;
            case '/staking':
                title = 'Stake';
                break;
        }

        return title;
    };

    const [menuTitle, setMenuTitle] = useState(getMenuTitle(router.pathname));

    const setMenuClassName = (pathname, itemPath) => {
        let className = 'menu-icon-link';

        if (pathname === itemPath) {
            className += ' active';
        }

        return className;
    };

    return (
        <>
            <Container>
                {ATTENTION === "" ? ("") : (
                    <div className="attention-title">
                        <span><marquee>{ATTENTION}</marquee></span>
                    </div>
                )}
                <div className="menu-icon-content">
                    <div className="header-btn">
                        {!isAuthenticated ? (
                            <Button onClick={handleClick}>
                                Connect Wallet
                            </Button>
                        ) : (
                            <span>
                                {truncate(user.attributes.accounts[0], 20)}
                                <Button onClick={handleLogout} className="ml-2">
                                    Disconnect
                                </Button>
                            </span>
                        )}
                    </div>
                    <div className="header-gla-contract">
                        <p>
                            <a href="https://bscscan.com/address/0x292Bb969737372E48D97C78c19B6a40347C33B45"
                                target="_blank" rel="noreferrer" alt="GLA Contract Address" title="GLA Contract Address"
                            >
                                0x292Bb969737372E48D97C78c19B6a40347C33B45</a>
                        </p>
                    </div>
                    <div className="header-user-profile">
                        {!isAuthenticated ? (
                            null
                        ) : (
                            <>
                                <span className="header-user-profile-text">
                                    <Image
                                        className="header-gla-icon"
                                        src={`/assets/images/logo/gla-coin.png`}
                                        alt="gla-coin.png"
                                        width={24}
                                        height={24}
                                    />
                                    &nbsp;{balanceToken}&nbsp;
                                </span>
                            </>
                        )}
                    </div>
                    <div className="header-user-refund">
                        {isAuthenticated && claimableBattleFee > 0 ? (
                            <div className="header-claim-div">
                                <span className="header-user-profile-text">
                                    <Image
                                        className="header-gla-icon"
                                        src={`/assets/images/logo/gla-coin.png`}
                                        alt="gla-coin.png"
                                        width={24}
                                        height={24}
                                    />
                                    <span>&nbsp;{claimableBattleFee}&nbsp;&nbsp;</span>
                                    <Button onClick={handleClaimClick}>
                                        Claim Battle Fee
                                    </Button>
                                    <div className="share-div">
                                        <div className="share-text">Share on</div>
                                        <OverlayTrigger
                                            placement="left"
                                            overlay={(props) => (
                                                <Tooltip id="button-tooltip" {...props}>
                                                    Twitter
                                                </Tooltip>)
                                            }>
                                            <TwitterShareButton
                                                url={shareUrl}
                                                title={title}
                                                hashtags={["Buildyoursquad",
                                                    "NFTGames",
                                                    "PlayToEarn",
                                                    "GalaxyAdventure",
                                                    "BSCGem",
                                                    "NFTMarketplace",
                                                    "GLA",
                                                    "Play2Earn"]}
                                                className="share-btn"
                                            >
                                                <TwitterIcon size={32} round />
                                            </TwitterShareButton>
                                        </OverlayTrigger>
                                        <OverlayTrigger
                                            placement="bottom"
                                            overlay={(props) => (
                                                <Tooltip id="button-tooltip" {...props}>
                                                    Facebook
                                                </Tooltip>)
                                            }>
                                            <FacebookShareButton
                                                url={shareUrl}
                                                quote={title}
                                                className="share-btn"
                                            >
                                                <FacebookIcon size={32} round />
                                            </FacebookShareButton>
                                        </OverlayTrigger>
                                        <OverlayTrigger
                                            placement="right"
                                            overlay={(props) => (
                                                <Tooltip id="button-tooltip" {...props}>
                                                    Telegram
                                                </Tooltip>)
                                            }>
                                            <TelegramShareButton
                                                url={shareUrl}
                                                title={title}
                                                className="share-btn"
                                            >
                                                <TelegramIcon size={32} round />
                                            </TelegramShareButton>
                                        </OverlayTrigger>
                                    </div>
                                </span>
                            </div>
                        ) : (null)}
                    </div>
                    <ul className="menu-icon">
                        <li className="menu-icon-item">
                            <Link href="/mybag">
                                <a
                                    className={setMenuClassName(
                                        router.pathname,
                                        '/mybag'
                                    )}
                                    onMouseEnter={() =>
                                        setMenuTitle('Inventory')
                                    }
                                    onMouseLeave={() =>
                                        setMenuTitle(
                                            getMenuTitle(router.pathname)
                                        )
                                    }
                                >
                                    <Image
                                        src="/assets/images/header/my-bag.png"
                                        alt="bag"
                                        width={100}
                                        height={100}
                                        layout="intrinsic"
                                    />
                                </a>
                            </Link>
                        </li>
                        <li className="menu-icon-item">
                            <Link href="/staking">
                                <a
                                    className={setMenuClassName(
                                        router.pathname,
                                        '/staking'
                                    )}
                                    onMouseEnter={() =>
                                        setMenuTitle('Staking')
                                    }
                                    onMouseLeave={() =>
                                        setMenuTitle(
                                            getMenuTitle(router.pathname)
                                        )
                                    }
                                >
                                    <Image
                                        src="/assets/images/header/staking.png"
                                        alt="staking"
                                        width={100}
                                        height={100}
                                        layout="intrinsic"
                                    />
                                </a>
                            </Link>
                        </li>
                        <li className="menu-icon-item">
                            <Link href="/">
                                <a
                                    className={setMenuClassName(
                                        router.pathname,
                                        '/'
                                    )}
                                    onMouseEnter={() => setMenuTitle('Store')}
                                    onMouseLeave={() =>
                                        setMenuTitle(
                                            getMenuTitle(router.pathname)
                                        )
                                    }
                                >
                                    <Image
                                        src="/assets/images/header/store.png"
                                        alt="store"
                                        width={100}
                                        height={100}
                                        layout="intrinsic"
                                    />
                                </a>
                            </Link>
                        </li>
                        <li className="menu-icon-item">
                            <Link href="/marketplace">
                                <a
                                    className={setMenuClassName(
                                        router.pathname,
                                        '/marketplace'
                                    )}
                                    onMouseEnter={() =>
                                        setMenuTitle('Marketplace')
                                    }
                                    onMouseLeave={() =>
                                        setMenuTitle(
                                            getMenuTitle(router.pathname)
                                        )
                                    }
                                >
                                    <Image
                                        src="/assets/images/header/market.png"
                                        alt="market"
                                        width={100}
                                        height={100}
                                        layout="intrinsic"
                                    />
                                </a>
                            </Link>
                        </li>
                        <li className="menu-icon-item">
                            <Link href="/pvp">
                                <a
                                    className={setMenuClassName(
                                        router.pathname,
                                        '/pvp'
                                    )}
                                    onMouseEnter={() =>
                                        setMenuTitle('PVP')
                                    }
                                    onMouseLeave={() =>
                                        setMenuTitle(
                                            getMenuTitle(router.pathname)
                                        )
                                    }
                                >
                                    <Image
                                        src="/assets/images/header/pvp.png"
                                        alt="pvp"
                                        width={100}
                                        height={100}
                                        layout="intrinsic"
                                    />
                                </a>
                            </Link>
                        </li>
                        <li className="menu-icon-item">
                            <Link href="/fight-monster">
                                <a
                                    className={setMenuClassName(
                                        router.pathname,
                                        '/fight-monster'
                                    )}
                                    onMouseEnter={() =>
                                        setMenuTitle('Battle')
                                    }
                                    onMouseLeave={() =>
                                        setMenuTitle(
                                            getMenuTitle(router.pathname)
                                        )
                                    }
                                >
                                    <Image
                                        src="/assets/images/header/fighting.png"
                                        alt="fighting"
                                        width={100}
                                        height={100}
                                        layout="intrinsic"
                                    />
                                </a>
                            </Link>
                        </li>
                    </ul>
                    <div className="menu-icon-title">
                        <span>{menuTitle}</span>
                    </div>
                </div>
            </Container>
        </>
    );
}
