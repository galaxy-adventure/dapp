import { useCallback, useEffect, useState } from "react";
import { Button, ButtonGroup, Col, Form, FormControl, InputGroup, Modal, Row, Table, ToggleButton, ToggleButtonGroup } from "react-bootstrap";
import Image from '../Image';
import { HERO_ABI, HERO_CONTRACT, STAKING_HERO_ABI, STAKING_HERO_CONTRACT } from "../../utils/const";
import { useMoralis } from "react-moralis";
import Swal from "sweetalert2";
import setLoading from "../../utils/loading";

const BASE_REWARD = 128;
const durationWin = new Map([
    [30, 45],
    [60, 110],
    [90, 180],
])
const plan = new Map([
    [30, 0],
    [60, 1],
    [90, 2],
])
const MIN_HERO_STAKE = 1
export default function StakingHero({ show, togleStaking }) {
    const { isAuthenticated, user, enableWeb3, isWeb3Enabled, web3 } = useMoralis();
    const [confirmEnable, setConfirmEnable] = useState(true);
    const [heroesData, setHeroesData] = useState([]);
    const [showDialog, setShowDialog] = useState(false);
    const [duration, setDuration] = useState(90);
    const [reward, setReward] = useState(1500);
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());

    const handleClose = () => {
        togleStaking();
    }

    const updateStartEndDate = () => {
        const offset = startDate.getTimezoneOffset();
        setStartDate(new Date());
        let edate = new Date(startDate.getTime() + (duration * 24 * 60 * 60 * 1000));
        setEndDate(edate);
    }

    const getReward = (data) => {
        const winBattle = durationWin.get(duration)
        const rarity = data.rarity;
        return winBattle * rarity * BASE_REWARD;
    }

    const updateReward = () => {
        let rws = 0;
        heroesData.map((data) => {
            if (data.checked) {
                rws = rws + getReward(data)
            }
        })
        setReward(rws);
    }

    const handleDurationChange = (val) => {
        setDuration(val);
    }

    const handleConfirmStaking = async () => {
        const stakeList = [];
        heroesData.map((data) => {
            if (data.checked === true) {
                stakeList.push(data.id);
            }
        })
        if (stakeList.length < MIN_HERO_STAKE) {
            Swal.fire("Not enough in Minimum Locked Amount", "", "warning");
            return;
        } else {
            setConfirmEnable(false);
            try {
                let isApproved = false;
                setLoading(true);
                const heroContract = new web3.eth.Contract(HERO_ABI, HERO_CONTRACT);
                await heroContract.methods.isApprovedForAll(user.get("ethAddress"), STAKING_HERO_CONTRACT)
                    .call().then((res) => {
                        if (res === true) {
                            isApproved = true;
                        }
                    })
                if (isApproved === false) {
                    await heroContract.methods.setApprovalForAll(STAKING_HERO_CONTRACT, true)
                        .send({ from: user.get("ethAddress") })
                        .then(() => {
                            // isApproved = true;
                        });
                }
                const stakePlan = plan.get(duration);
                const stakingContract = new web3.eth.Contract(STAKING_HERO_ABI, STAKING_HERO_CONTRACT);
                await stakingContract.methods
                    .stakeHero(stakeList, stakePlan)
                    .send({ from: user.get("ethAddress") })
                    .then((res) => {
                        setLoading(false);
                        Swal.fire("Staking HERO Successfully!", "", "success");
                    });
            } catch (err) {
                setLoading(false);
                Swal.fire("Staking HERO Error: " + err.message, "", "error");
                setConfirmEnable(true);
            }
        }
    }

    const loadHeroes = useCallback(
        async () => {
            const datas = [];
            try {
                const heroContract = new web3.eth.Contract(HERO_ABI, HERO_CONTRACT);
                await heroContract.methods
                    .getListHeroesOf(user.get("ethAddress"))
                    .call()
                    .then((res) => {
                        res.forEach((obj) => {
                            const hero = {};
                            hero.id = obj.heroId;
                            hero.types = parseInt(obj.types);
                            hero.name = obj.name;
                            hero.rarity = parseInt(obj.rarity);
                            hero.level = parseInt(obj.level);
                            hero.exp = parseFloat(obj.experience);
                            hero.checked = false;
                            datas.push(hero);
                        });
                        setHeroesData(datas);
                    });
            } catch (err) {
            } finally {
            }
        },
        [user]
    )
    const handleStakeSelected = (event, item) => {
        let isChecked = event.target.checked;
        // console.log(isChecked);
        const heroData = heroesData.map((data) => {
            if (data.id === item.id) {
                data.checked = isChecked;
            }
            return data;
        })
        setHeroesData(heroData);
    }
    useEffect(() => {
        setShowDialog(show);
        if (isAuthenticated && show) {
            loadHeroes();
        }
        setConfirmEnable(true);
    }, [show, isWeb3Enabled]);
    useEffect(() => {
        updateReward();
        updateStartEndDate();
    }, [show, duration, heroesData])
    return (
        <>
        <Modal
            show={showDialog}
            size="lg"
            className="modal-fighting"
            backdrop="static"
            keyboard={false}
            centered
            onHide={handleClose}
        >
        <Modal.Header closeButton>
          <Modal.Title>Stake HERO</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <div className="embed-responsive">
            <Row className="justify-content-center">
                <Col md={12} lg={12}>
                    <Image
                    src="/assets/images/pages/staking/gla_hero.png"
                    alt=""
                    width={64}
                    height={64}
                /></Col>
            </Row>
            <Row className="justify-content-center">
                <Col md={12} lg={12}>
                    <Form.Group className="mb-6" controlId="duration">
                        <Form.Label>Duration</Form.Label>
                        <InputGroup className="mb-6">
                            <ToggleButtonGroup name="duration" type="radio" value={duration} onChange={handleDurationChange}>
                                <ToggleButton id="tbg-btn-1" value={30}>
                                    30 Days
                                </ToggleButton>
                                <ToggleButton id="tbg-btn-2" value={60}>
                                    60 Days
                                </ToggleButton>
                                <ToggleButton id="tbg-btn-3" value={90}>
                                    90 Days
                                </ToggleButton>
                            </ToggleButtonGroup>
                        </InputGroup>
                    </Form.Group>
                </Col>
            </Row>
            <Row className="">
                <Col md={6} lg={6}>
                    <Form.Label>Est. Interests </Form.Label><span>&nbsp;{reward} GLA</span>
                </Col>
            </Row>
            <Row className="justify-content-center">
                <Col md={6} lg={6}>
                    <Form.Group className="mb-6" controlId="start-date">
                        <Form.Label>Start Date</Form.Label>
                        <InputGroup className="mb-6">
                            <span>{startDate.toLocaleDateString()}&nbsp;{startDate.toLocaleTimeString()}</span>
                        </InputGroup>
                    </Form.Group>
                </Col>
                <Col md={6} lg={6}>
                    <Form.Group className="mb-6" controlId="end-date">
                        <Form.Label>End Date</Form.Label>
                        <InputGroup className="mb-6">
                            <span>{endDate.toLocaleDateString()}&nbsp;{endDate.toLocaleTimeString()}</span>
                        </InputGroup>
                    </Form.Group>
                </Col>
            </Row>
            <Row className="justify-content-center">
                <Col md={12} lg={12}>
                    <Table responsive striped bordered hover variant="dark">
                        <thead>
                        <tr>
                            <th style={{ minWidth: 80 }}>#ID</th>
                            <th style={{ minWidth: 120 }}>Hero Name</th>
                            <th style={{ minWidth: 60 }}>Star</th>
                            <th style={{ minWidth: 70 }}>Level</th>
                            <th style={{ minWidth: 60 }}>EXP</th>
                            <th style={{ minWidth: 100 }}>Checked</th>
                        </tr>
                        </thead>
                        <tbody className=" list">
                        {heroesData.map((item, index) => (
                        <tr key={`hero_${item.id}`}>
                            <td>{item.id}</td>
                            <td>{item.name}</td>
                            <td>{item.rarity}</td>
                            <td>{item.level}</td>
                            <td>{item.exp}</td>
                            <td>
                                <input
                                    className="filter-checkbox-input"
                                    onChange={(e) =>
                                        handleStakeSelected(e, item)
                                    }
                                    type="checkbox"
                                    id={`filterRare${item.name}`}
                                    value={item.checked}
                                    checked={item.checked}
                                />
                            </td>
                        </tr>
                        ))}
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colSpan={5}>
                                <Form.Text className="text-muted">
                                Lock Amount Limitation: minimum {MIN_HERO_STAKE} NFT
                                </Form.Text>
                                </td>
                            </tr>
                        </tfoot>
                    </Table>
                </Col>
            </Row>
            <Row className="justify-content-center">
                <div className="d-grid gap-2">
                    <Button variant="primary" type="button" disabled={!confirmEnable} onClick={handleConfirmStaking}>
                        CONFIRM
                    </Button>
                </div>
            </Row>
        </div>
        </Modal.Body>
        </Modal>
        </>
    )
}
