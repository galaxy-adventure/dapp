import { useEffect, useState } from "react";
import { Button, ButtonGroup, Col, Form, FormControl, InputGroup, Modal, Row, ToggleButton, ToggleButtonGroup } from "react-bootstrap";
import Image from '../Image';
import { STAKING_ABI, STAKING_CONTRACT, TOKEN_ABI, TOKEN_CONTRACT } from "../../utils/const";
import { useMoralis } from "react-moralis";
import Swal from "sweetalert2";
import setLoading from "../../utils/loading";

const durationAPY = new Map([
    [30, 180],
    [60, 240],
    [90, 300],
])
const plan = new Map([
    [30, 0],
    [60, 1],
    [90, 2],
])
const MIN_GLA_STAKE = 10000
export default function StakingGLA({ show, togleStaking }) {
    const { isAuthenticated, user, enableWeb3, isWeb3Enabled, web3 } = useMoralis();
    const [confirmEnable, setConfirmEnable] = useState(true);
    const [showDialog, setShowDialog] = useState(false);
    const [duration, setDuration] = useState(90);
    const [estAPY, setEstAPY] = useState(300);
    const [reward, setReward] = useState(0);
    const [stakeAmount, setStakeAmount] = useState(0);
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [availableGLA, setAvailableGLA] = useState(0);

    const getBalanceToken = async () => {
        if (isAuthenticated) {
            const tokenContract = new web3.eth.Contract(TOKEN_ABI, TOKEN_CONTRACT);
            await tokenContract.methods
                .balanceOf(user.get("ethAddress"))
                .call()
                .then((res) => {
                    let balance = parseInt(res) / (10 ** 18);
                    setAvailableGLA(balance.toFixed(3));
                });
        }
    }

    const handleClose = () => {
        togleStaking();
    }

    const updateStartEndDate = () => {
        setStartDate(new Date());
        const offset = startDate.getTimezoneOffset();
        let edate = new Date(startDate.getTime() + (duration * 24 * 60 * 60 * 1000));
        setEndDate(edate);
    }

    const updateAPYandReward = () => {
        let rate = durationAPY.get(duration) / 100;
        let rw = (stakeAmount * rate / 365) * duration;
        setReward(rw);
    }

    const handleDurationChange = (val) => {
        setDuration(val);
        setEstAPY(durationAPY.get(val));
    }

    const handleChangeStakeAmount = (e, type) => {
        let value = e.target.value;
        setStakeAmount(parseFloat(value));
    }

    const handleConfirmStaking = async () => {
        if (stakeAmount < MIN_GLA_STAKE) {
            Swal.fire("Not enough in Minimum Locked Amount", "", "warning");
            return;
        } else if (stakeAmount > availableGLA) {
            Swal.fire("Your balance is not enough", "", "warning");
            return;
        } else {
            setConfirmEnable(false);
            try {
                let isApproved = false;
                setLoading(true);
                const tokenContract = new web3.eth.Contract(TOKEN_ABI, TOKEN_CONTRACT);
                await tokenContract.methods.allowance(user.get("ethAddress"), STAKING_CONTRACT).call().then((res) => {
                    let allowance = parseFloat(res) / (10 ** 18);
                    if (allowance >= stakeAmount) {
                        isApproved = true;
                    }
                });
                if (isApproved === false) {
                    let approveNumber = BigInt(1000000000 * (10 ** 18));
                    await tokenContract.methods.approve(STAKING_CONTRACT, approveNumber)
                        .send({ from: user.get("ethAddress") })
                        .then(() => {
                            // isApproved = true;
                        });
                }
                const stakingContract = new web3.eth.Contract(STAKING_ABI, STAKING_CONTRACT);
                let stakeAmountParam = BigInt(stakeAmount * (10 ** 18));
                let stakePlan = plan.get(duration);
                await stakingContract.methods
                    .stakeGLA(stakeAmountParam, stakePlan)
                    .send({ from: user.get("ethAddress") })
                    .then((res) => {
                        setLoading(false);
                        Swal.fire("Staking GLA Successfully!", "", "success");
                    });
            } catch (err) {
                setLoading(false);
                Swal.fire("Staking GLA Error: " + err.message, "", "error");
                setConfirmEnable(true);
            }
        }
    }
    useEffect(() => {
        setShowDialog(show);
        setDuration(90);
        setEstAPY(300);
        setReward(0);
        setStakeAmount(0);
        setConfirmEnable(true);
    }, [show])
    useEffect(() => {
        updateAPYandReward();
        updateStartEndDate();
        getBalanceToken();
    }, [stakeAmount, duration])
    return (
        <>
            <Modal
                show={showDialog}
                size="lg"
                className="modal-fighting"
                backdrop="static"
                keyboard={false}
                centered
                onHide={handleClose}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Stake GLA</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="embed-responsive">
                        <Row className="justify-content-center">
                            <Col md={12} lg={12}>
                                <Image
                                    src="/assets/images/pages/staking/gla_coin.png"
                                    alt=""
                                    width={64}
                                    height={64}
                                /></Col>
                        </Row>
                        <Row className="justify-content-center">
                            <Col md={12} lg={12}>
                                <Form.Group className="mb-6" controlId="duration">
                                    <Form.Label>Duration</Form.Label>
                                    <InputGroup className="mb-6">
                                        <ToggleButtonGroup name="duration" type="radio" value={duration} onChange={handleDurationChange}>
                                            <ToggleButton id="tbg-btn-1" value={30}>
                                                30 Days
                                            </ToggleButton>
                                            <ToggleButton id="tbg-btn-2" value={60}>
                                                60 Days
                                            </ToggleButton>
                                            <ToggleButton id="tbg-btn-3" value={90}>
                                                90 Days
                                            </ToggleButton>
                                        </ToggleButtonGroup>
                                    </InputGroup>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row className="justify-content-center">
                            <Col md={6} lg={6}>
                                <Form.Label>Est. APY </Form.Label><span>&nbsp;{estAPY}%</span>
                            </Col>
                            <Col md={6} lg={6}>
                                <Form.Label>Est. Interests </Form.Label><span>&nbsp;{reward} GLA</span>
                            </Col>
                        </Row>
                        <Row className="justify-content-center">
                            <Col md={6} lg={6}>
                                <Form.Group className="mb-6" controlId="start-date">
                                    <Form.Label>Start Date</Form.Label>
                                    <InputGroup className="mb-6">
                                        <span>{startDate.toLocaleDateString()}&nbsp;{startDate.toLocaleTimeString()}</span>
                                    </InputGroup>
                                </Form.Group>
                            </Col>
                            <Col md={6} lg={6}>
                                <Form.Group className="mb-6" controlId="end-date">
                                    <Form.Label>End Date</Form.Label>
                                    <InputGroup className="mb-6">
                                        <span>{endDate.toLocaleDateString()}&nbsp;{endDate.toLocaleTimeString()}</span>
                                    </InputGroup>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row className="justify-content-center">
                            <Col md={12} lg={12}>
                                <Form.Group className="mb-6" controlId="lock-amount">
                                    <div className="d-flex justify-content-between">
                                        <div><Form.Label>Lock Amount</Form.Label></div>
                                        <div><span className="justify-content-right">Available amount {availableGLA} GLA</span></div>
                                    </div>
                                    <InputGroup className="mb-6">
                                        <FormControl
                                            placeholder="Input Lock Amount"
                                            aria-label="Input Lock Amount"
                                            aria-describedby="basic-addon2"
                                            type="number"
                                            value={stakeAmount}
                                            onChange={(value) =>
                                                handleChangeStakeAmount(value)
                                            }
                                        />
                                        <InputGroup.Text id="basic-addon2">GLA</InputGroup.Text>
                                    </InputGroup>
                                    <Form.Text className="text-muted">
                                        Lock Amount Limitation: minimum {MIN_GLA_STAKE} GLA
                                    </Form.Text>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row className="justify-content-center">
                            <div className="d-grid gap-2">
                                <Button variant="primary" type="button" disabled={!confirmEnable} onClick={handleConfirmStaking}>
                                    CONFIRM
                                </Button>
                            </div>
                        </Row>
                    </div>
                </Modal.Body>
            </Modal>
        </>
    )
}