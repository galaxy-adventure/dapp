import Image from "../Image";
import React, { useEffect } from "react";
import { Col, ProgressBar } from "react-bootstrap";
import { useMoralis } from "react-moralis";
import Swal from "sweetalert2";
import StarRating from "../../components/Marketplace/StarRating";
import getHeroClass from "../../utils/app-util";
import {
  MARKET_ABI,
  MARKET_CONTRACT,
  TOKEN_ABI,
  TOKEN_CONTRACT,
} from "../../utils/const";
import setLoading from "../../utils/loading";
import { approve } from "../../utils/web3/approve";
import DisplayLastTimeBattle from "./LastTimeBattal";
import { truncate } from "../../utils/string";
import { EXP_THREESHOLD, RARE_MAX } from "../../utils/HeroData";

export default function Item({ data, setReloadPage }) {
  const { isAuthenticated, user, enableWeb3, isWeb3Enabled, web3 } =
    useMoralis();
  const {
    id,
    type,
    rarity,
    name,
    heroId,
    price,
    lastBattleTime,
    experience,
    level,
    owner,
  } = data;
  const truncatedName = truncate(name, 12);

  useEffect(() => {
    if (!isWeb3Enabled) {
      enableWeb3();
    }
  }, [web3.currentProvider]);

  function secondsToDhms(seconds) {
    const time = seconds * 1000;
    const newDate = new Date(time);
    return (
      newDate.toLocaleTimeString([], {
        hour: "2-digit",
        minute: "2-digit",
        hour12: false,
      }) +
      " " +
      newDate.toLocaleDateString()
    );
  }

  const handleWithdraw = async (heroId) => {
    Swal.fire({
      title: "Are you sure?",
      icon: "warning",
      showDenyButton: true,
      confirmButtonText: "Ok",
      denyButtonText: `Cancel`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        setLoading(true);
        try {
          const marketContract = new web3.eth.Contract(
            MARKET_ABI,
            MARKET_CONTRACT
          );
          await marketContract.methods
            .delistAndWithdraw(heroId)
            .send({ from: user.get("ethAddress") })
            .then(() => {
              setReloadPage(Math.random());
            });
        } catch (err) {
          setLoading(false);
          Swal.fire("Withdraw failed!", "", "error");
        }
      }
    });
  };

  // Add commas to price
  const priceRegex = price.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  const myEthAddress = isAuthenticated ? user.get("ethAddress") : null;

  const handleBuyHero = async (heroId) => {
    try {
      const marketContract = new web3.eth.Contract(MARKET_ABI, MARKET_CONTRACT);
      await marketContract.methods
        .getHeroPrice(heroId)
        .call()
        .then((res) => {
          // Find not found hero
          const heroPrice = parseInt(res);
          if (heroPrice <= 0) {
            Swal.fire(
              "Hero is NOT Listing. Please search again!",
              "",
              "warning"
            );
            return;
          } else {
            Swal.fire({
              title: "Are you sure?",
              icon: "warning",
              showDenyButton: true,
              confirmButtonText: "Ok",
              denyButtonText: `Cancel`,
            }).then(async (result) => {
              if (result.isConfirmed) {
                setLoading(true);
                const GLAToken = new web3.eth.Contract(
                  TOKEN_ABI,
                  TOKEN_CONTRACT
                );
                let allowance = await GLAToken.methods
                  .allowance(user.get("ethAddress"), MARKET_CONTRACT)
                  .call();
                if (allowance <= 0) {
                  let approveNumber = web3.utils.toWei(
                    BigInt(2 ** 64 - 1).toString(),
                    "ether"
                  );
                  await approve(
                    GLAToken,
                    {
                      name: "approve",
                      parameters: [MARKET_CONTRACT, approveNumber],
                    },
                    user.get("ethAddress")
                  );
                }
                // const marketplace = new web3.eth.Contract(MARKET_ABI, MARKET_CONTRACT);
                marketContract.once(
                  "Purchase",
                  {
                    filter: { heroId_: heroId },
                    fromBlock: "lastest",
                  },
                  (error, event) => {
                    // Reload hero
                    setLoading(false);
                    setReloadPage(Math.random());
                    Swal.fire("Buy Hero Successfully!", "", "success");
                  }
                );
                await marketContract.methods
                  .purchaseHero(heroId)
                  .send({ from: user.get("ethAddress") })
                  .then((res) => {
                    setLoading(false);
                  });
              }
            });
          }
        });
    } catch (error) {
      setLoading(false);
      Swal.fire("Buy hero failed!", "", "error");
    }
  };
  return (
    <Col key={id} md={6} lg={4}>
      <div className="card-shop">
        <div className="card-shop-ava">
          {owner === myEthAddress ? (
            <div className="card-shop-about">
              <Image
                key={`icon-tick-${data.id}`}
                src="/assets/images/logo/icon-tick.svg"
                width={58}
                height={58}
                alt="question"
              />
            </div>
          ) : null}
          <div className="card-shop-bg">
            <Image
              key={`base-${data.id}`}
              src="/assets/images/pages/store/base.png"
              width={372}
              height={142}
              alt="base"
            />
          </div>
          {type && rarity ? (
            <div className="card-shop-anim market-anim">
              <div className="card-shop-img">
                <Image
                  key={`hero-tick-${data.id}`}
                  src={`/assets/img/dapp/heros/${type}/R${rarity}.gif`}
                  width={200}
                  height={200}
                  alt="hero"
                />
              </div>
            </div>
          ) : null}
        </div>
        <div className="card-shop-meta">
          <div className="header-market">
            <div className="card-shop-name">{truncatedName}</div>
            <div className="card-shop-name market-id">#{heroId}</div>
          </div>
          <div className="card-shop-infor">
            <div className="card-shop-prices">
              <div className="card-shop-row">
                <StarRating rare={rarity} />
              </div>
              <div className="card-shop-row">
                <div className="card-shop-label">Price</div>
                <div className="card-shop-value card-price">
                  <div className="image-price">
                    <Image
                      key={`icon-coin-${data.id}`}
                      src={`/assets/images/logo/gla-coin.png`}
                      alt="gla-coin.svg"
                      width={16}
                      height={16}
                    />
                  </div>
                  <p>{priceRegex}</p>
                </div>
              </div>
              <div className="card-shop-row">
                <div className="card-shop-label">Level</div>
                <div className="card-shop-value">{level}</div>
              </div>
              <div className="card-shop-row">
                <div className="card-shop-label">EXP</div>
                <div className="card-shop-value">
                  {experience}
                  {level && level < RARE_MAX
                    ? ` / ${EXP_THREESHOLD[level - 1]}`
                    : null}
                </div>
              </div>
              <ProgressBar
                className="exp-bar"
                animated
                now={
                  level
                    ? [
                      level < RARE_MAX
                        ? (experience / EXP_THREESHOLD[level - 1]) * 100
                        : 100,
                    ]
                    : 0
                }
              />
              <div className="card-shop-row">
                <div className="card-shop-label">Class</div>
                <div className="card-shop-value">{getHeroClass(type)}</div>
              </div>
              <div className="card-shop-row">
                <div className="card-shop-label">Owner</div>
                <div className="card-shop-value owner-address">
                  {owner.substr(0, 4) +
                    "..." +
                    owner.substr(data.owner.length - 4)}
                </div>
              </div>
              <div className="card-shop-row market-row">
                <div className="card-shop-label">Last Battle</div>
                <div className="card-shop-value battle-time">
                  {lastBattleTime > 0 ? (
                    <DisplayLastTimeBattle battleTime={lastBattleTime * 1000} />
                  ) : (
                    "0"
                  )}
                </div>
              </div>
              {owner === myEthAddress ? (
                <div className="card-shop-action">
                  <button
                    className="card-shop-button d-block button-connect"
                    onClick={() => handleWithdraw(heroId)}
                  >
                    Withdraw
                  </button>
                  <a
                    href="https://pancakeswap.finance/swap?inputCurrency=BNB&outputCurrency=0x292Bb969737372E48D97C78c19B6a40347C33B45"
                    target="_blank"
                    className="card-shop-button d-block button-buy"
                    rel="noreferrer"
                  >
                    Buy GLA
                  </a>
                </div>
              ) : (
                <div className="card-shop-action">
                  <button
                    className="card-shop-button d-block button-connect"
                    onClick={() => handleBuyHero(heroId)}
                    disabled={owner === myEthAddress}
                  >
                    Buy
                  </button>
                  <a
                    href="https://pancakeswap.finance/swap?inputCurrency=BNB&outputCurrency=0x292Bb969737372E48D97C78c19B6a40347C33B45"
                    target="_blank"
                    className="card-shop-button d-block button-buy"
                    rel="noreferrer"
                  >
                    Buy GLA
                  </a>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </Col>
  );
}
