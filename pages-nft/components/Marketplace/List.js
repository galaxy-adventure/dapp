import React from "react";
import { Row, Col } from "react-bootstrap";
import Item from "./Item";
import Image from "../Image";

export default function List({ data, handleChangePage, setReloadPage }) {
  return (
    <div className="market-list">
      <Row className="justify-content-center">
        {data.map((item, index) => (
          <Item key={index} data={item} setReloadPage={setReloadPage}></Item>
        ))}
      </Row>
      <Row className="justify-content-center row">
        <Col className="market-footer">
          <hr />
          <div className="paging footer-paging">
            <button className="back-btn" onClick={() => handleChangePage("prev")}>
              <Image
                src="/assets/images/pages/marketplace/icon-back.png"
                width={100}
                height={50}
                alt="image"
              />
            </button>
            {/* <div className="total-page">10/{data.length}</div> */}
            <button className="next-btn" onClick={() => handleChangePage("next")}>
              <Image
                src="/assets/images/pages/marketplace/icon-next.png"
                width={100}
                height={50}
                alt="image"
              />
            </button>
          </div>
        </Col>
      </Row>
    </div>
  );
}
