import React, { useState } from "react";
import Image from "../Image";

const StarRating = ({ rare }) => {
  const rarityNumber = Number(rare);
  return (
    <div className="star-rating">
      {[...Array(rarityNumber)].map((star, index) => {
        index += 1;
        return (
          <Image
            key={index}
            src="/assets/images/stones/star1.png"
            width="33"
            height="32"
            layout="fixed"
            alt="image"
            className="star-rating--item"
          />
        );
      })}
      {[...Array(6 - rarityNumber)].map((star, index) => {
        index += 1;
        return (
          <Image
            key={index}
            src="/assets/images/stones/star2.png"
            width="33"
            height="32"
            layout="fixed"
            alt="image"
            className="star-rating--item"
          />
        );
      })}
    </div>
  );
};

export default StarRating;
