import React, { useState, useEffect, useContext } from "react";
import Image from "./Image";
import { useSound } from "use-sound";
import SoundContext from "../context/SoundContext";
import highVolume from "../public/assets/images/high-volume.png";
import muteVolume from "../public/assets/images/mute.png";

const Sound = () => {
  const soundURL = "/assets/sound/soundBG.mp3";
  const { soundStatus, setSoundStatus } = useContext(SoundContext);
  const [soundEnd, setSoundEnd] = useState(false);

  const [play, { stop }] = useSound(soundURL, {
    volume: 0.5,
    onend: () => {
      setSoundEnd(true);
    },
  });

  const handleChangeSound = () => {
    if (soundStatus) {
      setSoundStatus(false);
    } else {
      setSoundStatus(true);
    }
  };

  // Repeat sound when ending
  useEffect(() => {
    if (soundEnd && soundStatus) {
      play();
      setSoundEnd(false);
    }
  }, [soundEnd]);

  useEffect(() => {
    if (soundStatus) {
      play();
    } else {
      stop();
    }
  }, [soundStatus])

  return (
    <button className="sound-btn" onClick={() => handleChangeSound()}>
      {soundStatus ? (
        <Image src={highVolume} alt="high-volume" />
      ) : (
        <Image src={muteVolume} alt="mute-volume" />
      )}
    </button>
  );
};

export default Sound;
