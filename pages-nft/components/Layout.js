import React, { useState } from 'react';
import Header from './Header';
import Footer from './Footer';
import Head from 'next/head';
import Sound from './Sound';
import { SoundProvider } from '../context/SoundContext';

export default function Layout({ children }) {
    return (
        <div className="main-app">
            <Head>
                <title>Galaxy Adventure</title>
                <meta
                    name="viewport"
                    content="initial-scale=1.0, width=device-width"
                />

                <link
                    rel="apple-touch-icon"
                    sizes="180x180"
                    href="/assets/img/favicon/apple-touch-icon.png"
                />
                <link
                    rel="icon"
                    type="image/png"
                    sizes="32x32"
                    href="/assets/img/favicon/favicon-32x32.png"
                />
                <link
                    rel="icon"
                    type="image/png"
                    sizes="16x16"
                    href="/assets/img/favicon/favicon-16x16.png"
                />
                <link
                    rel="manifest"
                    href="/assets/img/favicon/site.webmanifest"
                />
                <link
                    rel="mask-icon"
                    href="/assets/img/favicon/safari-pinned-tab.svg"
                    color="#5bbad5"
                />
                <meta name="msapplication-TileColor" content="#da532c" />
                <meta name="theme-color" content="#ffffff" />
            </Head>
            <SoundProvider>
                <Header />
                {children}
                <Footer />
                <Sound />
            </SoundProvider>

            <div className="loading">
                <img src="/assets/images/loading.gif" alt="loading" />
            </div>
        </div>
    );
}
