import { useRouter } from "next/dist/client/router";
import Image from "../Image";
import React, { useState, useEffect } from "react";
import { Col, Row, Container, ProgressBar } from "react-bootstrap";
import { useMoralis } from "react-moralis";
import Swal from "sweetalert2";
import getHeroClass from "../../utils/app-util";
import {
  MARKET_ABI,
  MARKET_CONTRACT,
} from "../../utils/const";
import setLoading from "../../utils/loading";
import { truncate } from "../../utils/string";
import { RARE_MAX, EXP_THREESHOLD, timeCoolDowns } from "../../utils/HeroData";

export default function HeroPvp({ heroesData }) {
  const { user, web3, Moralis } = useMoralis();
  const [selectHeroIndex, setSelectHeroIndex] = useState(0);
  const [heroData, setHeroData] = useState({
    id: 0,
    type: 99,
    name: "",
    rarity: 0,
    level: 0,
    exp: 0,
  });
  const [ heroesPvP, setHeroesPvP ] = useState([]);
  const [pagination, setPagination] = useState({
    page: 1,
    pageSize: 10,
  });
  const handlePrev = () => {
    const nextIndex = selectHeroIndex - 1;
    if (nextIndex > -1) {
      setSelectHeroIndex(nextIndex);
    } else {
      setSelectHeroIndex(heroesData.length - 1);
    }
  };

  const handleNext = () => {
    const nextIndex = selectHeroIndex + 1;
    if (nextIndex < heroesData.length) {
      setSelectHeroIndex(nextIndex);
    } else {
      setSelectHeroIndex(0);
    }
  };

  const loadHeroPvP = async (inputParam) => {
    let query = {};
    let rareFilter = [];
    let levelFilter = [];
    if (inputParam.id != 0) {
      rareFilter.push(inputParam.rarity + "");
      levelFilter.push(inputParam.level + "")
    }
    if (rareFilter.length > 0) query.rarities = rareFilter;
    // if (levelFilter.length > 0) query.levels = levelFilter;
    const params = {
      page: pagination.page,
      pageSize: pagination.pageSize,
      filters: query,
    };
    setLoading(true);
    setHeroesPvP([]);
    try {
      let data = await Moralis.Cloud.run("getSaleHeroes", params);
      setHeroesPvP(data.data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      Swal.fire("Loading data error", "", "error");
    }
  }

  const warningAndFight = () => {

      Swal.fire({
        title: "Are you sure to fight?",
        html: "<span style='color:white;font-size:20'>Your win rate may be 45%</span>",
        icon: "warning",
        showCancelButton: true,
        cancelButtonColor: "#d33",
        confirmButtonColor: "#33cc33",
        confirmButtonText: "Fight",
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire("You WON!", "", "success");
        }
      });
  };

  useEffect(() => {
    const data = heroesData[selectHeroIndex];
    if (data) {
      setHeroData(data);
      loadHeroPvP(data);
    } else {
      const temp = {
        id: 0,
        type: 99,
        name: "",
        rarity: 0,
        level: 0,
        exp: 0,
      };
      setHeroData(temp);
      loadHeroPvP(temp);
    }
  }, [selectHeroIndex]);

  const truncatedName = truncate(heroData.name, 12);

  const handleSelectRare = (types, rare) => {
    return `/assets/img/dapp/heros/${types}/R${rare}.gif`;
  };
  const createStarImage = (onOff, index) => (
    <Image
      key={`star-${index}`}
      id={`star-${index}`}
      src={`/assets/images/stones/star${onOff}.png`}
      width={32}
      height={32}
      alt="Star"
    />
  );
  const showStars = (rare) => {
    let stars = [];
    for (var i = 0; i < rare; i++) {
      stars.push(1);
    }
    for (var i = rare; i < 6; i++) {
      stars.push(2);
    }
    return stars.map(createStarImage);
  };
  return (
    <div className="page-content page-fight-monster">
      <Container>
        <div className="page-header">
          <Row className="justify-content-center">
            <Col sm={12} md={6} lg={4}>
              <div className="hero-skills">
                <img
                  className="main-skill"
                  src={`/assets/images/pages/fight-monster/type${heroData.type}.png`}
                />
                <img
                  className="skills"
                  src="/assets/images/pages/fight-monster/SKill.png"
                />
              </div>
            </Col>
            <Col sm={12} md={6} lg={4}>
              <div className="card-shop">
                <div className="card-shop-ava">
                  <div className="card-shop-bg">
                    <Image
                      src="/assets/images/pages/fight-monster/beHero.png"
                      width={320}
                      height={370}
                      alt="base"
                    />
                  </div>
                  <div className="card-shop-anim">
                    <div className="card-shop-img hero-img">
                      <Image
                        id={`hero-${heroData.id}`}
                        src={handleSelectRare(heroData.type, heroData.rarity)}
                        width={200}
                        height={200}
                        alt={heroData.name}
                      />
                    </div>
                  </div>
                  <div className="card-shop-prev back-btn">
                    <button onClick={() => handlePrev()}>Prev</button>
                  </div>
                  <div className="card-shop-next next-btn">
                    <button onClick={() => handleNext()}>Next</button>
                  </div>
                </div>
              </div>
            </Col>
            <Col sm={12} md={6} lg={3} col-md-offset={3} className="mr-lg-auto">
              <div className="card-shop">
                <div className="card-shop-meta">
                  <div className="header-card">
                    <div className="card-shop-name">{truncatedName}</div>
                    {heroData.name && (
                      <div className="index-number">
                        #{selectHeroIndex + 1}/{heroesData.length}
                      </div>
                    )}
                  </div>
                  <div className="card-shop-infor">
                    <div className="card-shop-prices">
                      <div className="card-shop-row">
                        {showStars(heroData.rarity)}
                      </div>
                      <div className="card-shop-row">
                        <div className="card-shop-label">Level</div>
                        <div className="card-shop-value">{heroData.level}</div>
                      </div>
                      <div className="card-shop-row">
                        <div className="card-shop-label">Experience</div>
                        <div className="card-shop-value">
                          {heroData.exp}
                        </div>
                      </div>
                      <div className="card-shop-row">
                        <div className="card-shop-label">Class</div>
                        <div className="card-shop-value">
                          {getHeroClass(heroData.type)}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </div>
        <div className="fight-pvp-list">
        <h3 className="fight-pvp-title">Chosen One</h3>
        <Row className="justify-content-center">
        {heroesPvP.map((heroPvP, index) => (
          <Col key={heroPvP.heroId} md={6} lg={3}>
            <div className="card-shop">
              <div className="card-shop-ava">
                <div className="card-shop-bg">
                  <Image
                    key={`frame-${heroPvP.heroId}`}
                    id={`frame-${heroPvP.heroId}`}
                    src="/assets/images/pages/store/base.png"
                    width={372}
                    height={142}
                    alt="base"
                  />
                </div>
                <div className="card-shop-anim">
                  <div className="card-shop-img">
                    <Image
                      key={`hero-${heroPvP.heroId}`}
                      id={`hero-${heroPvP.heroId}`}
                      src={`/assets/img/dapp/heros/${heroPvP.type}/R${heroPvP.rarity}.gif`}
                      width={400}
                      height={400}
                      alt="Hero"
                    />
                  </div>
                </div>
              </div>
              <div className="card-shop-meta">
                <div className="header-card">
                  <div className="card-shop-name">{heroPvP.name}</div>
                  <div className="index-number">#{heroPvP.heroId}</div>
                </div>
                <div className="card-shop-infor">
                  <div className="card-shop-prices">
                    <div className="card-shop-row">{showStars(heroPvP.rarity)}</div>
                    <div className="card-shop-row">
                      <div className="card-shop-label">Level</div>
                      <div className="card-shop-value">{heroPvP.level}</div>
                    </div>
                    <div className="card-shop-row">
                      <div className="card-shop-label">Experience</div>
                      <div className="card-shop-value">
                        {heroPvP.experience}
                      </div>
                    </div>
                    <div className="card-shop-row">
                      <div className="card-shop-label">Class</div>
                      <div className="card-shop-value">
                        {getHeroClass(heroPvP.type)}
                      </div>
                    </div>
                    <div className="card-shop-row">
                      <div className="card-shop-label">Bet Amount</div>
                      <div className="card-shop-value">
                        <Image
                          key={`gla-coin-${heroPvP.heroId}`}
                          id={`gla-coin-${heroPvP.heroId}`}
                          src="/assets/images/logo/gla-coin.png"
                          width={18}
                          height={18}
                          alt="GLA Coin"
                        />&nbsp;{heroPvP.price}
                      </div>
                    </div>
                  </div>
                </div>
                  <div className="card-shop-action">
                    <button
                      className="card-shop-button d-block button-buy"
                      onClick={() => warningAndFight(heroData)}
                    >
                      Fight
                    </button>
                  </div>
              </div>
            </div>
          </Col> 
        ))}
        </Row>
        </div>
      </Container>
    </div>
  );
}
