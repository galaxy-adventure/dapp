import React, { createContext, useState } from "react";

const SoundContext = createContext();

export const SoundProvider = ({ children }) => {
  const [soundStatus, setSoundStatus] = useState(false);

  return (
    <SoundContext.Provider
      value={{ soundStatus, setSoundStatus }}
    >
      {children}
    </SoundContext.Provider>
  );
};

export default SoundContext;
