import { Button, Container, Nav, Tab } from "react-bootstrap";
import { Table, Media, Row, Col, Form, FormControl, InputGroup } from 'react-bootstrap';
import Image from '../components/Image';
import { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { STAKING_ABI, STAKING_CONTRACT, TOKEN_ABI, TOKEN_CONTRACT } from "../utils/const";
import { useMoralis } from "react-moralis";
import setLoading from "../utils/loading";
import HeroPvp from "../components/Pvp/hero-pvp";

const heroesData = [
    {
        id: 10001,
        name: 'RAY',
        image: '/assets/img/dapp/heros/0/R1.gif',
        type: 0,
        rarity: 1,
        level: 1,
        exp: 40,
        bet: 50000,
        class: 'Navy Seal',
    },
    {
        id: 10002,
        name: 'JAX',
        image: '/assets/img/dapp/heros/1/R1.gif',
        type: 1,
        rarity: 2,
        level: 1,
        exp: 809,
        bet: 25000,
        class: 'Mercenary',
    },
    {
        id: 10003,
        name: 'KAT',
        image: '/assets/img/dapp/heros/2/R1.gif',
        type: 2,
        rarity: 3,
        level: 2,
        exp: 2574,
        bet: 28000,
        class: 'Assassin',
    },
];
export default function Pvp() {
    const { isAuthenticated, user, enableWeb3, isWeb3Enabled, web3, Moralis } = useMoralis();
    const [ availableGLA2, setAvailableGLA2 ] = useState(0);
    const [action, setAction] = useState(0);

    const getBalanceToken2 = async () => {
        if (isAuthenticated) {
            const tokenContract = new web3.eth.Contract(TOKEN_ABI, TOKEN_CONTRACT);
            await tokenContract.methods
                .balanceOf(user.get("ethAddress"))
                .call()
                .then((res) => {
                    let balance = parseInt(res) / (10 ** 18);
                    setAvailableGLA2(balance.toFixed(3));
                });
        }
    }
    const handleWithdraw = async (item) => {
        try {
            let curDateTime = new Date();
            if (curDateTime.getTime() < item.endDate.getTime()) {
                Swal.fire({
                    title: "Redeem Before Maturity Date!",
                    html: "<span style='color:white;'>You Will NOT Earn Any Reward If Redeeming Before Maturity Date!</span>",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Redeem Before Maturity Date",
                }).then((result) => {
                    if (result.isConfirmed) {
                        redeem(item);
                    }
                });
            } else {
                setLoading(true);
                const stakingContract = new web3.eth.Contract(STAKING_ABI, STAKING_CONTRACT);
                await stakingContract.methods
                    .unstakeGLA(item.id)
                    .send({ from: user.get("ethAddress") })
                    .then((res) => {
                        loadStakingList();
                        setLoading(false);
                    });
            }
        } catch (err) {
            setLoading(false);
        }
    }

    useEffect(() => {
        if (!isWeb3Enabled) {
            enableWeb3();
        }
        if (isAuthenticated) {
            getBalanceToken2();
        }
        // loadHeroPvP();
    }, [isWeb3Enabled, user]);

    return (
        <div className="page-content page-starter-pack">
            <Container>
                <Tab.Container id="pvp-tabs-example" defaultActiveKey="pvp">
                <div className="menu-icon-child">
                    <Nav variant="pills" className="menu-child">
                    <Nav.Item className="menu-child-item">
                        <Nav.Link eventKey="pvp" className="menu-child-link" onMouseDown={() =>
                                        setAction(1)
                                    }>
                        Player
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item className="menu-child-item">
                        <Nav.Link eventKey="pve" className="menu-child-link" onMouseDown={() =>
                                        setAction(2)
                                    }>
                        PVP
                        </Nav.Link>
                    </Nav.Item>
                    </Nav>
                </div>
                <Tab.Content>
                    <Tab.Pane eventKey="pvp">
                        <Row className="align-items-center">
                            <Col sm={3} className="my-1">
                                <InputGroup className="mb-2">
                                    <FormControl type="numer" id="pvp_wallet" disabled placeholder="PVP Wallet" />
                                    <InputGroup.Text>GLA</InputGroup.Text>
                                </InputGroup>
                            </Col>
                            <Col xs="auto">
                                <Button type="button" className="mb-2">
                                    Withdraw
                                </Button>
                            </Col>
                        </Row>

                        <Row>
                            <Col sm={3} className="my-1">
                                <div className="d-flex justify-content-between">
                                    <div><span className="justify-content-right">Available amount {availableGLA2} GLA</span></div>
                                </div>
                            </Col>
                        </Row>
                        <Row className="align-items-center">
                            <Col sm={3} className="my-1">
                                <InputGroup className="mb-2">
                                    <FormControl  type="number" step="100" min="100" id="token_input" placeholder="Token Input" />
                                    <InputGroup.Text>GLA</InputGroup.Text>
                                </InputGroup>
                            </Col>
                            <Col xs="auto">
                                <Button type="button" className="mb-2">
                                    Add GLA to PVP Wallet
                                </Button>
                            </Col>
                        </Row>
                        <Row>
                        <Table responsive striped bordered hover variant="dark" style={{ minWidth: 400 }}>
                        <thead>
                            <tr>
                                <th className="sort" data-sort="product" scope="col" style={{ minWidth: 160 }}>
                                    Hero
                                </th>
                                <th className="sort" data-sort="estapy" scope="col" style={{ minWidth: 100 }}>
                                    Star
                                </th>
                                <th className="sort" data-sort="estreward" scope="col" style={{ minWidth: 140 }}>
                                    Level
                                </th>
                                <th className="sort" data-sort="duration" scope="col" style={{ minWidth: 140 }}>
                                    EXP
                                </th>
                                <th className="sort" data-sort="duration" scope="col" style={{ minWidth: 140 }}>
                                    Bet Amount
                                </th>
                                <th scope="col" style={{ minWidth: 140 }}>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody className=" list">
                        {heroesData.map((item, index) => (
                            <tr key={index}>
                                <th scope="row">
                                    <Media className=" align-items-center">
                                        <a
                                        className=" avatar rounded-circle mr-3"
                                        href="#"
                                        onClick={(e) => e.preventDefault()}
                                        >
                                        <Image
                                            src={`/assets/img/dapp/heros/${item.type}/R${item.rarity}.gif`}
                                            alt={item.name}
                                            width={64}
                                            height={64}
                                        />
                                        </a>
                                        <Media>
                                        <span className="name mb-0 text-sm">
                                            {item.name}
                                        </span>
                                        </Media>
                                    </Media>
                                </th>
                                <td className="estapy"><div className="pt-20">{item.rarity}</div></td>
                                <td className="estreward"><div className="pt-20">{item.level}</div></td>
                                <td className="estreward"><div className="pt-20">{item.exp}</div></td>
                                <td className="estreward"><div className="pt-20">
                                    <FormControl type="number" step="1" min="1000" id={`bet-amount-${index}`} placeholder="Bet Amount" />
                                    </div></td>
                                <td className="operation">
                                <div className="pt-20">
                                    <button
                                        className="btn btn-warning btn-block"
                                            onClick={() => handleStaking(item.type)}
                                    >
                                        Join PVP
                                    </button>
                                </div>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                        </Table>

                        </Row>
                    </Tab.Pane>
                    <Tab.Pane eventKey="pve" className="section-title">
                        <HeroPvp heroesData={heroesData}></HeroPvp>
                    </Tab.Pane>
                </Tab.Content>
                </Tab.Container>
            </Container>
        </div>
    )
}
