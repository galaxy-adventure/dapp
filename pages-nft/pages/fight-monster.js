import Image from "../components/Image";
import { useRouter } from "next/router";
import React, { useCallback, useEffect, useState, useRef } from "react";
import { Col, Container, Row, ProgressBar } from "react-bootstrap";
import { useMoralis } from "react-moralis";
import Countdown from "../components/FightMonster/Countdown";
import ListFightMonster from "../components/FightMonster/ListFightMonster";
import getHeroClass from "../utils/app-util";
import { HERO_ABI, HERO_CONTRACT } from "../utils/const";
import { truncate } from "../utils/string";
import { RARE_MAX, EXP_THREESHOLD, timeCoolDowns } from "../utils/HeroData";

const monsterFight = [
  {
    name: "Fireball",
    level: 1,
    winRate: 80,
    rewardEstimated: "128~2764",
    expEstimated: "10~216",
    image: "/assets/img/dapp/enemy/2/L1.gif",
    available: true,
  },

  {
    name: "Brainbot",
    level: 2,
    winRate: 60,
    rewardEstimated: "153~3317",
    expEstimated: "12~259",
    image: "/assets/img/dapp/enemy/1/L1.gif",
    available: true,
  },
  {
    name: "Blaze",
    level: 3,
    winRate: 40,
    rewardEstimated: "230~4976",
    expEstimated: "18~389",
    image: "/assets/img/dapp/enemy/0/L1.gif",
    available: true,
  },
  {
    name: "Morph",
    level: 4,
    winRate: 20,
    rewardEstimated: "384~8294",
    expEstimated: "30~648",
    image: "/assets/img/dapp/enemy/3/L1.gif",
    available: true,
  },
];

export default function FightMonster() {
  const { isAuthenticated, user, enableWeb3, isWeb3Enabled, web3 } =
    useMoralis();
  const router = useRouter();
  const [heroesData, setHeroesData] = useState([]);
  const [heroData, setHeroData] = useState({
    id: 0,
    types: 99,
    name: "",
    rare: 0,
    level: 0,
    exp: 0,
    lastBattleTime: 0,
    remainTime: 0,
  });
  const [remainTime, setRemainTime] = useState(0);
  const [endTime, setEndTime] = useState(0);
  const [selectHeroIndex, setSelectHeroIndex] = useState(0);
  const [monsters, setMonsters] = useState(monsterFight.slice(0, 4));
  const [fightBtn, setFightBtn] = useState(false);
  const [reloadPage, setReloadPage] = useState(0);
  const firstRun = useRef(true);

  const getTimeCountdown = (hero) => {
    let remainTime;
    let heroTimeCoolDown = timeCoolDowns[Number(hero.rare) - 1] * 1000; // ms
    const timeToPlay = hero.lastBattleTime * 1000 + heroTimeCoolDown; // ms
    if (Date.now() > timeToPlay) {
      remainTime = 0;
    } else {
      remainTime = timeToPlay - Date.now();
    }
    setEndTime(timeToPlay);
    setRemainTime(remainTime);
    // Update hero remainTime
    hero.remainTime = remainTime;
    setHeroData(hero);
  };

  const handleSelectRare = (types, rare) => {
    return `/assets/img/dapp/heros/${types}/R${rare}.gif`;
  };
  const createStarImage = (onOff, index) => (
    <Image
      key={`star-${index}`}
      id={`star-${index}`}
      src={`/assets/images/stones/star${onOff}.png`}
      width={32}
      height={32}
      alt="Star"
    />
  );
  const showStars = (rare) => {
    let stars = [];
    for (var i = 0; i < rare; i++) {
      stars.push(1);
    }
    for (var i = rare; i < 6; i++) {
      stars.push(2);
    }
    return stars.map(createStarImage);
  };

  const handlePrev = () => {
    const nextIndex = selectHeroIndex - 1;
    if (nextIndex > -1) {
      setSelectHeroIndex(nextIndex);
    } else {
      setSelectHeroIndex(heroesData.length - 1);
    }
  };

  const handleNext = () => {
    const nextIndex = selectHeroIndex + 1;
    if (nextIndex < heroesData.length) {
      setSelectHeroIndex(nextIndex);
    } else {
      setSelectHeroIndex(0);
    }
  };

  const loadHeroes = useCallback(
    async (heroContract) => {
      const datas = [];
      try {
        await heroContract.methods
          .getListHeroesOf(user.get("ethAddress"))
          .call()
          .then((res) => {
            res.forEach((obj) => {
              const hero = {};
              hero.id = obj.heroId;
              hero.types = parseInt(obj.types);
              hero.name = obj.name;
              hero.rare = parseInt(obj.rarity);
              hero.level = parseInt(obj.level);
              hero.exp = parseFloat(obj.experience);
              hero.price = obj.price ? obj.price : 7000;
              hero.lastBattleTime = parseInt(obj.lastBattleTime);
              datas.push(hero);
            });
            setHeroesData(datas);
            if (datas.length > 0) {
              setHeroData(datas[0]);
              setSelectHeroIndex(0);
              getTimeCountdown(datas[0]);
            }
          });
      } catch (err) {
        // console.log(err);
      } finally {
      }
    },
    [user]
  );

  useEffect(() => {
    if (!isWeb3Enabled) {
      enableWeb3();
    }
    if (isAuthenticated) {
      const heroContract = new web3.eth.Contract(HERO_ABI, HERO_CONTRACT);
      loadHeroes(heroContract);
    }
  }, [isWeb3Enabled]);

  useEffect(() => {
    if (router.pathname.includes("/fight-monster")) {
      let mainapp = document.querySelector(".main-app");
      mainapp.style.background =
        'url("/assets/images/pages/fight-monster/bg1.png") top center no-repeat,url("/assets/images/pages/fight-monster/bg2.png") top center repeat';
      mainapp.style.backgroundSize = "100%";
    }
    return () => {
      let mainapp = document.querySelector(".main-app");
      mainapp.style.background = "";

      mainapp.style.backgroundSize = "100%";
    };
  }, [router.pathname]);

  // Reload hero when user click prev and next btn
  // This is because the closure problem in useState
  useEffect(() => {
    const updateCountDown = () => {
      const data = heroesData[selectHeroIndex];
      if (data) {
        setHeroData(data);
        getTimeCountdown(data);
      } else {
        const temp = {
          id: 0,
          types: 99,
          name: "",
          rare: 0,
          level: 0,
          exp: 0,
          lastBattleTime: 0,
          remainTime: 0,
        };
        setHeroData(temp);
        getTimeCountdown(temp);
      }
    };
    updateCountDown();
  }, [reloadPage, selectHeroIndex]);

  // Reset when user disconnect account and connect again
  useEffect(() => {
    // Not run if user reload page -> web3 not enable yet
    if (isWeb3Enabled) {
      // Handle case user switch between page -> don't run in first run
      // Avoid duplicated loadHeroes and Items
      if (firstRun.current) {
        firstRun.current = false;
      } else if (!user) {
        setHeroesData([]);
        setHeroData({
          id: 0,
          types: 99,
          name: "",
          rare: 0,
          level: 0,
          exp: 0,
          lastBattleTime: 0,
          remainTime: 0,
        });
        setEndTime(0); // Reset next battle time to 0
        setRemainTime(1); // Make btn fight spin
      } else {
        const heroContract = new web3.eth.Contract(HERO_ABI, HERO_CONTRACT);
        loadHeroes(heroContract);
      }
    }
  }, [user]);

  useEffect(() => {
    if (fightBtn) {
      setRemainTime(0);
      setFightBtn(false);
    }
  }, [fightBtn]);

  const reloadHero = () => {
    setReloadPage(Math.floor(Math.random() * 101));
  };

  const truncatedName = truncate(heroData.name, 12);

  return (
    <div className="page-content page-fight-monster">
      <Container>
        <div className="page-header">
          <Row className="justify-content-center">
            <Col sm={12} md={6} lg={4}>
              <div className="hero-skills">
                <img
                  className="main-skill"
                  src={`/assets/images/pages/fight-monster/type${heroData.types}.png`}
                />
                <img
                  className="skills"
                  src="/assets/images/pages/fight-monster/SKill.png"
                />
              </div>
            </Col>
            <Col sm={12} md={6} lg={4}>
              <div className="card-shop">
                <div className="card-shop-ava">
                  <div className="card-shop-bg">
                    <Image
                      src="/assets/images/pages/fight-monster/beHero.png"
                      width={320}
                      height={370}
                      alt="base"
                    />
                  </div>
                  <div className="card-shop-anim">
                    <div className="card-shop-img hero-img">
                      <Image
                        id={`hero-${heroData.id}`}
                        src={handleSelectRare(heroData.types, heroData.rare)}
                        width={200}
                        height={200}
                        alt={heroData.name}
                      />
                    </div>
                  </div>
                  <div className="card-shop-prev back-btn">
                    <button onClick={() => handlePrev()}>Prev</button>
                  </div>
                  <div className="card-shop-next next-btn">
                    <button onClick={() => handleNext()}>Next</button>
                  </div>
                </div>
              </div>
            </Col>
            <Col sm={12} md={6} lg={3} col-md-offset={3} className="mr-lg-auto">
              <div className="card-shop">
                <div className="card-shop-meta">
                  <div className="header-card">
                    <div className="card-shop-name">{truncatedName}</div>
                    {heroData.name && (
                      <div className="index-number">
                        #{selectHeroIndex + 1}/{heroesData.length}
                      </div>
                    )}
                  </div>
                  <div className="card-shop-infor">
                    <div className="card-shop-prices">
                      <div className="card-shop-row">
                        {showStars(heroData.rare)}
                      </div>
                      <div className="card-shop-row">
                        <div className="card-shop-label">Level</div>
                        <div className="card-shop-value">{heroData.level}</div>
                      </div>
                      <div className="card-shop-row">
                        <div className="card-shop-label">Experience</div>
                        <div className="card-shop-value">
                          {heroData.exp}
                          {heroData.level && heroData.level < RARE_MAX
                            ? ` / ${EXP_THREESHOLD[heroData.level - 1]}`
                            : null}
                        </div>
                      </div>
                      <ProgressBar
                        className="exp-bar"
                        animated
                        now={
                          heroData.level
                            ? [
                              heroData.level < RARE_MAX
                                ? (heroData.exp /
                                  EXP_THREESHOLD[heroData.level - 1]) *
                                100
                                : 100,
                            ]
                            : 0
                        }
                      />
                      <div className="card-shop-row">
                        <div className="card-shop-label">Class</div>
                        <div className="card-shop-value">
                          {getHeroClass(heroData.types)}
                        </div>
                      </div>
                      <div className="card-shop-row last-item">
                        <div className="card-shop-label">Next Battle</div>
                        <div className="card-shop-value">
                          <Countdown
                            endTime={endTime}
                            setFightBtn={setFightBtn}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </div>
        <div className="fight-monster-list">
          <ListFightMonster
            data={monsters}
            heroData={heroData}
            remainTime={remainTime}
            reloadHero={reloadHero}
          ></ListFightMonster>
        </div>
      </Container>
    </div>
  );
}
