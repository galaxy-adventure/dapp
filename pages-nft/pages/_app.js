import Layout from "../components/Layout";

import "bootstrap/dist/css/bootstrap.min.css";
import "../public/assets/css/fontawesome-all.min.css";
import "../public/assets/css/animate.css";
import "../styles/globals.css";
import "../public/assets/css/responsive.css";
import "../styles/pages.css";
import "../styles/starterPack.css";
import "../styles/marketplace.css";
import "../styles/fight-monster.css";
import "../styles/custom.css";
import "../styles/mybag.css";
import "../styles/header.css";
import "../styles/modal.css";
import { APP_KEY, APP_URL } from "../utils/const";
import { MoralisProvider } from "react-moralis";

const MyApp = ({ Component, pageProps }) => {
  return (
    <MoralisProvider appId={APP_KEY} serverUrl={APP_URL}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </MoralisProvider>
  );
};

export default MyApp;
