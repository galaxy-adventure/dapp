import React, { useCallback, useEffect, useState, useRef } from "react";
import { Container, Nav, Tab } from "react-bootstrap";
import { useMoralis } from "react-moralis";
import List from "../components/MyBag/List";
import {
  HERO_ABI,
  HERO_CONTRACT,
  ITEM_ABI,
  ITEM_CONTRACT,
  MARKET_ABI,
  MARKET_CONTRACT,
} from "../utils/const";

export default function MyBag() {
  const { isAuthenticated, user, enableWeb3, isWeb3Enabled, web3 } =
    useMoralis();
  const [heroesData, setHeroesData] = useState([]);
  const [itemsData, setItemsData] = useState([]);
  const [heroData, setHeroData] = useState(0);
  const firstRun = useRef(true);

  const changeHeroData = (arg) => {
    setHeroData(arg);
  };

  const loadHeroes = useCallback(async () => {
    const datas = [];
    try {
      // Get hero list
      const heroContract = new web3.eth.Contract(HERO_ABI, HERO_CONTRACT);
      await heroContract.methods
        .getListHeroesOf(user.get("ethAddress"))
        .call()
        .then((res) => {
          res.forEach((obj) => {
            const hero = {};
            hero.id = parseInt(obj.heroId);
            hero.types = parseInt(obj.types);
            hero.name = obj.name;
            hero.rare = parseInt(obj.rarity);
            hero.level = parseInt(obj.level);
            hero.exp = parseFloat(obj.experience);
            hero.price = obj.price ? parseInt(obj.price) : 7000;
            hero.lastBattleTime = parseInt(obj.lastBattleTime);
            hero.listMarket = false;
            datas.push(hero);
          });
        });
      // Get hero on market
      const marketContract = new web3.eth.Contract(MARKET_ABI, MARKET_CONTRACT);
      await marketContract.methods
        .getListHeroesOf(user.get("ethAddress"))
        .call()
        .then((res) => {
          res.forEach((obj) => {
            const retHero = obj.hero;
            const hero = {};
            hero.id = parseInt(retHero.heroId);
            hero.types = parseInt(retHero.types);
            hero.name = retHero.name;
            hero.rare = parseInt(retHero.rarity);
            hero.level = parseInt(retHero.level);
            hero.exp = parseFloat(retHero.experience);
            hero.price = obj.price ? parseInt(obj.price) : 7000;
            hero.lastBattleTime = parseInt(retHero.lastBattleTime);
            hero.listMarket = true;
            datas.push(hero);
          });
        });
    } catch (err) {
      // console.log(err);
    } finally {
      setHeroesData(datas);
    }
  }, [isWeb3Enabled, user]);

  const loadItems = useCallback(async () => {
    const datas = [];
    try {
      // Get GEM & CORE & Chest
      const itemContract = new web3.eth.Contract(ITEM_ABI, ITEM_CONTRACT);
      await itemContract.methods
        .balanceOf(user.get("ethAddress"), 1)
        .call()
        .then((res) => {
          const item = {};
          const types = 1;
          item.types = types;
          item.amount = parseInt(res);
          item.name = "Gem";
          item.image = "/assets/images/pages/store/gem_gif.gif";
          datas.push(item);
        });
      await itemContract.methods
        .balanceOf(user.get("ethAddress"), 2)
        .call()
        .then((res) => {
          const item = {};
          const types = 2;
          item.types = types;
          item.amount = parseInt(res);
          item.name = "Core";
          item.image = "/assets/images/pages/store/core_gif.gif";
          datas.push(item);
        });
      await itemContract.methods
        .balanceOf(user.get("ethAddress"), 3)
        .call()
        .then((res) => {
          const item = {};
          const types = 3;
          item.types = types;
          item.amount = parseInt(res);
          item.name = "Chest";
          item.image = "/assets/images/pages/store/chest_gif.gif";
          datas.push(item);
        });
    } catch (err) {
      // console.log(err);
    } finally {
      setItemsData(datas);
    }
  }, [isWeb3Enabled, user]);

  useEffect(() => {
    if (!isWeb3Enabled) {
      enableWeb3();
    }
    if (isAuthenticated) {
      loadHeroes();
      loadItems();
    }
  }, [heroData, isWeb3Enabled]);

  // Reset when user disconnect account
  useEffect(() => {
    // Not run if user reload page -> web3 not enable yet
    if (isWeb3Enabled) {
      // Handle case user switch between page -> don't run in first run
      // Avoid duplicated loadHeroes and Items
      if (firstRun.current) {
        firstRun.current = false;
      } else if (!user) {
        setHeroesData([]);
        setItemsData([]);
      } else {
        loadHeroes();
        loadItems();
      }
    }
  }, [user]);

  return (
    <div className="page-content page-starter-pack">
      <Container>
        <Tab.Container id="left-tabs-example" defaultActiveKey="heroes">
          <div className="menu-icon-child">
            <Nav variant="pills" className="menu-child">
              <Nav.Item className="menu-child-item">
                <Nav.Link eventKey="heroes" className="menu-child-link">
                  Heroes
                </Nav.Link>
              </Nav.Item>
              <Nav.Item className="menu-child-item">
                <Nav.Link eventKey="items" className="menu-child-link">
                  Items
                </Nav.Link>
              </Nav.Item>
            </Nav>
          </div>

          <Tab.Content>
            <Tab.Pane eventKey="heroes">
              <List
                data={heroesData}
                itemsData={itemsData}
                changeHeroData={changeHeroData}
                types={1}
              ></List>
            </Tab.Pane>
            <Tab.Pane eventKey="items">
              <List
                data={itemsData}
                changeHeroData={changeHeroData}
                types={2}
              ></List>
            </Tab.Pane>
          </Tab.Content>
        </Tab.Container>
      </Container>
    </div>
  );
}
