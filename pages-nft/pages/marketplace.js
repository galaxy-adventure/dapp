import Image from "../components/Image";
import React, { useEffect, useState, useMemo } from "react";
import { DebounceInput } from "react-debounce-input";
import {
  Button,
  Col,
  Container,
  Row,
  InputGroup,
  FormControl,
} from "react-bootstrap";
import { useMoralis } from "react-moralis";
import Swal from "sweetalert2";
import List from "../components/Marketplace/List";
import setLoading from "../utils/loading";

export default function Marketplace() {
  const { enableWeb3, isWeb3Enabled, web3, Moralis } = useMoralis();
  const [pagination, setPagination] = useState({
    page: 1,
    pageSize: 10,
  });
  const [reloadPage, setReloadPage] = useState(0);
  const [listMarket, setListMarket] = useState([]);
  const [sortType, setSortType] = useState("asc");
  const [inputName, setInputName] = useState("");
  const [nameSearch, setNameSearch] = useState("");
  const [classHero, setClassHero] = useState([
    { name: "Navy Seal", status: false, type: 0 },
    { name: "Mercenary", status: false, type: 1 },
    { name: "Assassin", status: false, type: 2 },
  ]);
  const [rareHero, setRareHero] = useState([
    {
      name: 1,
      status: false,
    },
    {
      name: 2,
      status: false,
    },
    {
      name: 3,
      status: false,
    },
    {
      name: 4,
      status: false,
    },
    {
      name: 5,
      status: false,
    },
    {
      name: 6,
      status: false,
    },
  ]);
  const [levelHero, setLevelHero] = useState([
    {
      name: 1,
      status: false,
    },
    {
      name: 2,
      status: false,
    },
    {
      name: 3,
      status: false,
    },
    {
      name: 4,
      status: false,
    },
    {
      name: 5,
      status: false,
    },
    {
      name: 6,
      status: false,
    },
  ]);

  useEffect(() => {
    if (!isWeb3Enabled) {
      enableWeb3();
    }
  }, [web3.currentProvider]);

  useEffect(() => {
    setLoading(true);
    search();
  }, [
    reloadPage,
    pagination.page,
    nameSearch,
    JSON.stringify(levelHero),
    JSON.stringify(rareHero),
    JSON.stringify(classHero),
  ]);

  const handleSelectFilter = (event, item, type) => {
    let dataRaw = type === "level" ? levelHero : rareHero;
    let isChecked = event.target.checked;
    const updateDataHero = dataRaw.map((c) => {
      if (c.name === item.name) {
        c.status = isChecked;
        return c;
      }
      return c;
    });
    if (type === "level") {
      setLevelHero(updateDataHero);
    } else {
      setRareHero(updateDataHero);
    }
  };
  const debounce = (func, wait, immediate) => {
    let timeout;

    return function executedFunction() {
      let context = this;
      let args = arguments;

      let later = function () {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };

      let callNow = immediate && !timeout;

      clearTimeout(timeout);

      timeout = setTimeout(later, wait);

      if (callNow) func.apply(context, args);
    };
  };

  const search = debounce(async () => {
    let query = {};
    const rareFilter = rareHero.reduce((acc, item) => {
      if (item.status) return acc.concat(String(item.name));
      return acc;
    }, []);
    const levelFilter = levelHero.reduce((acc, i) => {
      if (i.status) return acc.concat(String(i.name));
      return acc;
    }, []);
    const classFilter = classHero.reduce((acc, v) => {
      if (v.status) return acc.concat(String(v.type));
      return acc;
    }, []);
    setLoading(true);
    if (rareFilter.length > 0) query.rarities = rareFilter;
    if (levelFilter.length > 0) query.levels = levelFilter;
    if (classFilter.length > 0) query.types = classFilter;
    if (nameSearch) query.name = nameSearch;

    const params = {
      page: pagination.page,
      pageSize: pagination.pageSize,
      filters: query,
    };

    // console.log("params", params);
    try {
      let data = await Moralis.Cloud.run("getSaleHeroes", params);
      setListMarket(data.data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      Swal.fire("Loading data error", "", "error");
    }
  }, 500);
  const handleChangeClass = (item) => {
    const updateClassHero = classHero.map((c) => {
      if (c.type === item.type) {
        c.status = !item.status;

        return c;
      }

      return c;
    });
    setClassHero(updateClassHero);
  };

  const handleChangePage = (action) => {
    if (action === "prev") {
      setPagination({
        ...pagination,
        page: pagination.page > 0 ? pagination.page - 1 : 1,
      });
    } else {
      setPagination({ ...pagination, page: pagination.page + 1 });
    }
  };

  const handleReloadList = () => {
    setPagination({ page: 1, pageSize: 10 });
  };

  const handleResetClick = () => {
    const resetH = rareHero.map((item) => ({ ...item, status: false }));
    setRareHero(resetH);
    setLevelHero(levelHero.map((item) => ({ ...item, status: false })));
    setClassHero(classHero.map((item) => ({ ...item, status: false })));
  };

  const handleSearchName = (e) => {
    let userInput = e.target.value;
    debouncedChangeHandler(userInput);
  };

  const debouncedChangeHandler = useMemo(
    () => debounce((userInput) => setNameSearch(userInput), 500),
    []
  );

  const handlePriceSort = () => {
    setSortType((prevState) => {
      return prevState === "asc" ? "des" : "asc";
    });

    if (sortType === "asc") {
      listMarket.sort((a, b) => Number(a.price) - Number(b.price));
    } else {
      listMarket.sort((a, b) => Number(b.price) - Number(a.price));
    }

    setListMarket(listMarket);
  };

  return (
    <div className="page-content page-starter-pack">
      <div className="page-content page-starter-pack">
        <Container>
          <Row className="justify-content-center">
            <Col lg={12} xl={4} className="mb-4">
              <div className="market-search">
                <div className="market-search__filter">
                  <div>
                    <InputGroup className="mb-3">
                      <DebounceInput
                        type="text"
                        debounceTimeout={500}
                        onChange={(event) => setNameSearch(event.target.value)}
                        placeholder="Search name"
                        className="market-search__name"
                      />
                    </InputGroup>
                  </div>
                  <div className="form-group">
                    <h5>Star</h5>
                    <div className="filter-checkbox-wrap filter-col-3">
                      {rareHero.map((label) => (
                        <div key={label.name} className="filter-checkbox">
                          <input
                            className="filter-checkbox-input"
                            onChange={(e) =>
                              handleSelectFilter(e, label, "rare")
                            }
                            type="checkbox"
                            id={`filterRare${label.name}`}
                            value={label.status}
                            checked={label.status}
                          />
                          <label
                            className="filter-checkbox-label"
                            htmlFor={`filterRare${label.name}`}
                          >
                            {label.name}
                          </label>
                          <Image
                            src="/assets/images/stones/star1.png"
                            width={32}
                            height={32}
                            alt="image"
                          />
                        </div>
                      ))}
                    </div>
                  </div>
                  <div className="form-group">
                    <h5>Level</h5>
                    <div className="filter-checkbox-wrap filter-col-3">
                      {levelHero.map((label) => (
                        <div key={label.name} className="filter-checkbox">
                          <input
                            className="filter-checkbox-input"
                            onChange={(e) =>
                              handleSelectFilter(e, label, "level")
                            }
                            type="checkbox"
                            id={`filterLevel${label.name}`}
                            value={label.status}
                            checked={label.status}
                          />
                          <label
                            className="filter-checkbox-label"
                            htmlFor={`filterLevel${label.name}`}
                          >
                            {label.name}
                          </label>
                        </div>
                      ))}
                    </div>
                  </div>
                  <div className="form-group">
                    <h5>Class</h5>
                    <div className="filter-checkbox-wrap filter-col-3 filter-checkbox-class">
                      {classHero.map((item, index) => {
                        return (
                          <div key={index} className="filter-class">
                            <Image
                              src={`/assets/images/pages/marketplace/${item.type
                                }-${item.status ? "active" : "inactive"}.png`}
                              width={100}
                              height={100}
                              alt="image"
                              onClick={() => handleChangeClass(item)}
                            />
                            <p>{item.name.toLocaleUpperCase()}</p>
                          </div>
                        );
                      })}
                    </div>
                  </div>
                </div>
                <div className="market-search__suggest">
                  <button
                    className="card-shop-button d-block button-buy"
                    onClick={handlePriceSort}
                  >
                    Price sort{" "}
                    {sortType === "asc" ? (
                      <Image
                        src={`/assets/img/icon-sort-down.svg`}
                        width={20}
                        height={20}
                        alt="image"
                      />
                    ) : (
                      <Image
                        src={`/assets/img/icon-sort-up.svg`}
                        width={20}
                        height={20}
                        alt="image"
                      />
                    )}
                  </button>

                  <button
                    className="card-shop-button d-block button-connect"
                    onClick={handleResetClick}
                  >
                    Reset
                  </button>
                </div>
              </div>
            </Col>
            {listMarket && listMarket.length > 0 ? (
              <Col lg={12} xl={8}>
                <Container>
                  <List
                    data={listMarket}
                    handleChangePage={handleChangePage}
                    setReloadPage={setReloadPage}
                  ></List>
                </Container>
              </Col>
            ) : (
              <Col lg={12} xl={8}>
                <Container>
                  <h2>No hero here...</h2>
                  <Button onClick={() => handleReloadList()}>
                    Find more hero
                  </Button>
                </Container>
              </Col>
            )}
          </Row>
        </Container>
      </div>
    </div>
  );
}
