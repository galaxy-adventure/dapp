import { Container, Nav, Tab } from "react-bootstrap";
import { Table, Media, Row, Col } from 'react-bootstrap';
import Image from '../components/Image';
import StakingGLA from "../components/Staking/staking-gla";
import StakingHero from "../components/Staking/staking-hero";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { STAKING_ABI, STAKING_CONTRACT, STAKING_HERO_ABI, STAKING_HERO_CONTRACT } from "../utils/const";
import { useMoralis } from "react-moralis";
import setLoading from "../utils/loading";

const BASE_REWARD = 128;
const planMap = new Map([
    ["0", 30],
    ["1", 60],
    ["2", 90],
])
const winBattleMap = new Map([
    ["0", 45],
    ["1", 110],
    ["2", 180],
])
const productStakingData = [
    {
        type: "stakingGLA",
        product: "GLA",
        estAPY: 3,
        winAmount: "-",
        duration: 90,
        icon: "/assets/images/pages/staking/gla_coin.png"
    },
    {
        type: "stakingHero",
        product: "HERO",
        estAPY: 0,
        winAmount: "5760 ~ 138240 GLA",
        duration: 90,
        icon: "/assets/images/pages/staking/gla_hero.png"
    }
]
export default function Staking() {
    const { isAuthenticated, user, enableWeb3, isWeb3Enabled, web3 } = useMoralis();
    const [showGLA, setShowGLA] = useState(false);
    const [showHero, setShowHero] = useState(false);
    const [stakedData, setStakedData] = useState([]);
    const [actionValue, setActionValue] = useState(0);

    const openStakingGLA = () => {
        setShowGLA(!showGLA);
    }
    const openStakingHero = () => {
        setShowHero(!showHero);
    }
    const handleStaking = (type) => {
        if (type === 'stakingGLA') {
            openStakingGLA();
        } else {
            openStakingHero();
        }
    }
    const handleWithdrawOrRedeem = async (item) => {
        withdraw(item);
    }
    const redeem = async (item) => {
        try {
            if (item.type === 'stakingGLA') {
                setLoading(true);
                const stakingContract = new web3.eth.Contract(STAKING_ABI, STAKING_CONTRACT);
                await stakingContract.methods
                    .unstakeGLA(item.id)
                    .send({ from: user.get("ethAddress") })
                    .then((res) => {
                        loadStakingList();
                        setLoading(false);
                    });
            }
        } catch (err) {
            setLoading(false);
        }
    }
    const withdraw = async (item) => {
        try {
            if (item.type === 'stakingGLA') {
                let curDateTime = new Date();
                /* let afterMaturity = false;
                const stakingContract = new web3.eth.Contract(STAKING_ABI, STAKING_CONTRACT);
                await stakingContract.methods._afterMaturity(item.id)
                    .call()
                    .then((res) => {
                        afterMaturity = res;
                    }); */
                if (curDateTime.getTime() < item.endDate.getTime()) {
                    Swal.fire({
                        title: "Redeem Before Maturity Date!",
                        html: "<span style='color:white;'>You Will NOT Earn Any Reward If Redeeming Before Maturity Date!</span>",
                        icon: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#3085d6",
                        cancelButtonColor: "#d33",
                        confirmButtonText: "Redeem Before Maturity Date",
                    }).then((result) => {
                        if (result.isConfirmed) {
                            redeem(item);
                        }
                    });
                } else {
                    setLoading(true);
                    const stakingContract = new web3.eth.Contract(STAKING_ABI, STAKING_CONTRACT);
                    await stakingContract.methods
                        .unstakeGLA(item.id)
                        .send({ from: user.get("ethAddress") })
                        .then((res) => {
                            loadStakingList();
                            setLoading(false);
                        });
                }
            } else {
                let curDateTime = new Date();
                if (curDateTime.getTime() < item.endDate.getTime()) {
                    Swal.fire({
                        title: "Unstake Before Maturity Date!",
                        html: "<span style='color:white;'>Unable to Withdraw Hero Before Maturity Date!</span>",
                        icon: "warning"
                    });
                } else {
                    setLoading(true);
                    const stakingHeroContract = new web3.eth.Contract(STAKING_HERO_ABI, STAKING_HERO_CONTRACT);
                    await stakingHeroContract.methods
                        .withdraw(item.id)
                        .send({ from: user.get("ethAddress") })
                        .then((res) => {
                            loadStakingList();
                            setLoading(false);
                        });
                }
            }
        } catch (err) {
            setLoading(false);
        }
    }
    const loadStakingList = async () => {
        try {
            let datas = [];
            // setLoading(true);
            const stakingContract = new web3.eth.Contract(STAKING_ABI, STAKING_CONTRACT);
            await stakingContract.methods
                .getUserDepositInfo(user.get("ethAddress"))
                .call()
                .then((res) => {
                    // console.log("GLA");
                    // console.log(res);
                    res.forEach((obj) => {
                        const data = {};
                        data.type = "stakingGLA";
                        data.product = "GLA";
                        data.id = parseInt(obj.depositIndex);
                        data.estAPY = parseFloat(obj.percent) / 100;
                        data.amountDeposit = parseFloat(obj.amountDeposit) / (10 ** 18);
                        // data.winAmount = 0;
                        let rewardEstimate = parseFloat(obj.amountGain) / (10 ** 18);
                        data.rewardEstimate = rewardEstimate.toFixed(3);
                        data.duration = planMap.get(obj.plan);
                        data.startDate = new Date(parseInt(obj.start) * 1000);
                        data.endDate = new Date(parseInt(obj.finish) * 1000);
                        let curDateTime = new Date();
                        if (curDateTime.getTime() < data.endDate.getTime()) {
                            data.redeemEarlier = true;
                        } else {
                            data.redeemEarlier = false;
                        }
                        data.icon = "/assets/images/pages/staking/gla_coin.png";
                        datas.push(data);
                    });
                    // setLoading(false);
                });

            const stakingHeroContract = new web3.eth.Contract(STAKING_HERO_ABI, STAKING_HERO_CONTRACT);
            await stakingHeroContract.methods
                .listHeroData(user.get("ethAddress"))
                .call()
                .then((res) => {
                    // console.log("HERO");
                    // console.log(res);
                    res.forEach((obj) => {
                        const data = {};
                        data.type = "stakingHero";
                        data.product = "HERO #" + obj.heroId;
                        data.id = parseInt(obj.heroId);
                        data.estAPY = parseFloat(obj.percent) / 100;
                        data.amountDeposit = 0;
                        // data.winAmount = winBattleMap.get(obj.plan);
                        let rewardEstimate = parseFloat(obj.rewardEstimate) / (10 ** 18);
                        data.rewardEstimate = rewardEstimate.toFixed(3);
                        data.duration = planMap.get(obj.plan);
                        data.startDate = new Date(parseInt(obj.start) * 1000);
                        data.endDate = new Date(parseInt(obj.finish) * 1000);
                        data.redeemEarlier = false;
                        data.icon = "/assets/images/pages/staking/gla_hero.png";
                        datas.push(data);
                    });
                    // setLoading(false);
                });
            setStakedData(datas);
        } catch (err) {
            // setLoading(false);
            Swal.fire("Staking GLA Error: " + err.message, "", "error");
        }
    }
    const setAction = (action) => {
        setActionValue(action);
    }
    useEffect(() => {
        if (!isWeb3Enabled) {
            enableWeb3();
        }
        if (isAuthenticated && actionValue === 2) {
            setStakedData([]);
            loadStakingList();
        }
    }, [isWeb3Enabled, user, actionValue, showGLA, showHero]);

    return (
        <div className="page-content page-starter-pack">
            <Container>
                <Tab.Container id="staking-tabs-example" defaultActiveKey="stakelist">
                <div className="menu-icon-child">
                    <Nav variant="pills" className="menu-child">
                    <Nav.Item className="menu-child-item">
                        <Nav.Link eventKey="stakelist" className="menu-child-link" onMouseDown={() =>
                                        setAction(1)
                                    }>
                        Stake
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item className="menu-child-item">
                        <Nav.Link eventKey="mystaking" className="menu-child-link" onMouseDown={() =>
                                        setAction(2)
                                    }>
                        Staking
                        </Nav.Link>
                    </Nav.Item>
                    </Nav>
                </div>

                <Tab.Content>
                    <Tab.Pane eventKey="stakelist">
                        <h3>Locked Stake</h3>
                        <Table responsive striped bordered hover variant="dark" style={{ minWidth: 400 }}>
                        <thead>
                            <tr>
                                <th className="sort" data-sort="product" scope="col" style={{ minWidth: 160 }}>
                                    Product
                                </th>
                                <th className="sort" data-sort="estapy" scope="col" style={{ minWidth: 100 }}>
                                    Est. APY
                                </th>
                                <th className="sort" data-sort="estreward" scope="col" style={{ minWidth: 140 }}>
                                    Est. Rewards
                                </th>
                                <th className="sort" data-sort="duration" scope="col" style={{ minWidth: 140 }}>
                                    Duration
                                </th>
                                <th scope="col" style={{ minWidth: 140 }}>Operation</th>
                            </tr>
                        </thead>
                        <tbody className=" list">
                        {productStakingData.map((item, index) => (
                            <tr key={index}>
                                <th scope="row">
                                    <Media className=" align-items-center">
                                        <a
                                        className=" avatar rounded-circle mr-3"
                                        href="#"
                                        onClick={(e) => e.preventDefault()}
                                        >
                                        <Image
                                            src={item.icon}
                                            alt={item.product}
                                            width={64}
                                            height={64}
                                        />
                                        </a>
                                        <Media>
                                        <span className=" name mb-0 text-sm">
                                            {item.product}
                                        </span>
                                        </Media>
                                    </Media>
                                </th>
                                <td className="estapy"><div className="pt-20">{item.estAPY > 0 ? ( `${item.estAPY * 100}%` ) : ( "-" ) }</div></td>
                                <td className="estreward"><div className="pt-20">{item.winAmount}</div></td>
                                <td className="duration"><div className="pt-20">{item.duration} Days</div></td>
                                <td className="operation">
                                <div className="pt-10">
                                    <button
                                        className="btn btn-warning btn-block"
                                            onClick={() => handleStaking(item.type)}
                                    >
                                        Stake Now
                                    </button>
                                </div>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                        </Table>
                    </Tab.Pane>
                    <Tab.Pane eventKey="mystaking">
                        <h3>My Staking</h3>
                        <Table responsive striped bordered hover variant="dark" style={{ minWidth: 400 }}>
                        <thead>
                            <tr>
                                <th className="sort" data-sort="product" scope="col" style={{ minWidth: 160 }}>
                                    Product
                                </th>
                                <th className="sort" data-sort="estapy" scope="col" style={{ minWidth: 100 }}>
                                    Est. APY
                                </th>
                                <th className="sort" data-sort="amountDeposit" scope="col" style={{ minWidth: 140 }}>
                                    Deposited
                                </th>
                                <th className="sort" data-sort="estreward" scope="col" style={{ minWidth: 140 }}>
                                    Est. Rewards
                                </th>
                                <th className="sort" data-sort="duration" scope="col" style={{ minWidth: 120 }}>
                                    Duration
                                </th>
                                <th className="sort" data-sort="startdate" scope="col" style={{ minWidth: 120 }}>
                                    Start Date
                                </th>
                                <th className="sort" data-sort="enddate" scope="col" style={{ minWidth: 120 }}>
                                    End Date
                                </th>
                                <th scope="col" style={{ minWidth: 140 }}>Operation</th>
                            </tr>
                        </thead>
                        <tbody className="list">
                        {stakedData.map((item, index) => (
                            <tr key={index}>
                                <th scope="row">
                                    <Media className=" align-items-center">
                                        <a
                                        className=" avatar rounded-circle mr-3"
                                        href="#"
                                        onClick={(e) => e.preventDefault()}
                                        >
                                        <Image
                                            src={item.icon}
                                            alt={item.product}
                                            width={64}
                                            height={64}
                                        />
                                        </a>
                                        <Media>
                                        <span className=" name mb-0 text-sm">
                                            {item.product}
                                        </span>
                                        </Media>
                                    </Media>
                                </th>
                                <td className="estapy"><div className="pt-20">{item.estAPY > 0 ? ( `${item.estAPY * 100}%` ) : ( "-" ) }</div></td>
                                <td className="amountDeposit"><div className="pt-20">{item.amountDeposit > 0 ? ( `${item.amountDeposit}` ) : ( "-" ) }</div></td>
                                {/* <td className=" winamount"><div className="pt-20">{item.winAmount > 0 ? ( `${item.winAmount}` ) : ( "-" ) }</div></td> */}
                                <td className="winamount"><div className="pt-20">{item.rewardEstimate > 0 ? ( `${item.rewardEstimate}` ) : ( "-" ) }</div></td>
                                <td scop="row">
                                    <div className="pt-20">
                                        {item.duration}&nbsp;Days
                                    </div>
                                </td>
                                <td className="startdate"><div className="pt-20">{item.startDate.toLocaleDateString()}&nbsp;{item.startDate.toLocaleTimeString()}</div></td>
                                <td className="enddate"><div className="pt-20">{item.endDate.toLocaleDateString()}&nbsp;{item.endDate.toLocaleTimeString()}</div></td>
                                {/* <td className=" status"><div className="pt-20">Holding</div></td> */}
                                <td className="operation">
                                <div className="pt-10">
                                    <button
                                        className="btn btn-warning btn-block"
                                            onClick={() => handleWithdrawOrRedeem(item)}
                                    >
                                        {item.redeemEarlier? "Withdraw" : "Withdraw"}
                                    </button>
                                </div>
                                </td>
                            </tr>
                        ))}
                        </tbody>
                        </Table>
                    </Tab.Pane>
                </Tab.Content>
                </Tab.Container>
            </Container>
            <StakingGLA show={showGLA} togleStaking={openStakingGLA}></StakingGLA>
            <StakingHero show={showHero} togleStaking={openStakingHero}></StakingHero>
        </div>
    )
}
