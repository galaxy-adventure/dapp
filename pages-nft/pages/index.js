import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Tab, Nav, Form } from 'react-bootstrap';
import Image from '../components/Image';
import { useRouter } from 'next/router';
import { useMoralis } from 'react-moralis';
import {
    SPAWN_ABI,
    SPAWN_CONTRACT,
    HERO_TYPE,
    ITEM_ABI,
    ITEM_CONTRACT, TOKEN_ABI, TOKEN_CONTRACT,
} from '../utils/const';
import Swal from 'sweetalert2';
import setLoading from '../utils/loading';
import { approve } from '../utils/web3/approve'


const packData = [
    {
        name: 'RAY',
        price: '7,000',
        bnb: '',
        busd: '',
        image: '/assets/img/dapp/heros/0/R1.gif',
        type: 'hero',
        hero: 0,
        level: 1,
        class: 'Navy Seal',
    },
    {
        name: 'JAX',
        price: '7,000',
        paymentMethod: {
            bnb: '0.62217',
            busd: '263.99',
        },
        type: 'hero',
        image: '/assets/img/dapp/heros/1/R1.gif',
        hero: 1,
        level: 1,
        class: 'Mercenary',
    },
    {
        name: 'KAT',
        price: '7,000',
        bnb: '',
        busd: '',
        image: '/assets/img/dapp/heros/2/R1.gif',
        type: 'hero',
        hero: 2,
        level: 1,
        class: 'Assassin',
    },
];

const packItemData = [
    {
        name: 'Gem',
        defaultAmount: 100,
        price: '10',
        created: '14,000',
        limit: '14,000',
        isHavePayment: false,
        bnb: '',
        busd: '',
        image: '/assets/images/pages/store/gem_gif.gif',
        type: 'box',
        hero: 2,
    },
    {
        name: 'Core',
        defaultAmount: 1,
        price: '1,000',
        created: '14,000',
        limit: '14,000',
        isHavePayment: false,
        bnb: '',
        busd: '',
        image: '/assets/images/pages/store/core_gif.gif',
        type: 'box',
        hero: 3,
    },
    {
        name: 'Chest',
        defaultAmount: 1,
        price: '10,000',
        bnb: '',
        busd: '',
        image: '/assets/images/pages/store/chest_gif.gif',
        type: 'box',
        hero: 3,
    },
];

const getImagePack = (pack) => {
    return (
        <Image
            src={pack.image}
            alt={pack.name}
            className="gem-img"
            width={150}
            height={150}
        />
    );
};

const PaymentMethodSelect = ({ pack }) => {
    const [paymentMethod, changePayment] = useState(<></>);

    return (
        <>
            {paymentMethod}
            <div className="price">
                <div className="price-title">Payment</div>
                <div className="price-value form-group">
                    <div className="purchase input-group">
                        <select
                            className="form-control"
                            onChange={(event) => {
                                if (event.target.value !== 'gla') {
                                    changePayment(
                                        <PaymentMethodNormalIcon
                                            title={
                                                event.target.options[
                                                    event.target.selectedIndex
                                                ].text
                                            }
                                            value={
                                                pack.paymentMethod[
                                                event.target.value
                                                ]
                                            }
                                            icon={`/assets/images/logo/${event.target.value}.svg`}
                                        ></PaymentMethodNormalIcon>
                                    );
                                } else {
                                    changePayment(<></>);
                                }
                            }}
                        >
                            <option value="gla">GLA</option>
                            <option value="bnb">BNB</option>
                            <option value="busd">BUSD</option>
                        </select>
                    </div>
                </div>
            </div>
        </>
    );
};

const PaymentMethodNormal = ({ title, value, icon }) => (
    <div className="price">
        <div className="price-title">{title}</div>
        <div className="price-value">
            <div className="d-inline-flex align-items-center">
                <span>{value}</span>
            </div>
        </div>
    </div>
);

const PaymentMethodNormalIcon = ({ title, value, icon }) => (
    <div className="price">
        <div className="price-title">{title}</div>
        <div className="price-value">
            <Image src={icon} alt="coin-icon" width={24} height={23} />
            <div className="d-inline-flex align-items-center">
                <span>{value}</span>
            </div>
        </div>
    </div>
);

const showPayment = (pack) => {
    if (pack.isHavePayment) {
        return <PaymentMethodSelect pack={pack}></PaymentMethodSelect>;
    } else {
        return (
            <>
                <PaymentMethodNormal
                    title="Created"
                    value={pack.created}
                ></PaymentMethodNormal>
                <PaymentMethodNormal
                    title="Limit"
                    value={pack.limit}
                ></PaymentMethodNormal>
            </>
        );
    }
};

export default function StarterPack() {
    const {
        authenticate,
        isAuthenticated,
        user,
        enableWeb3,
        isWeb3Enabled,
        web3,
        Moralis
    } = useMoralis();
    const ethAddress = user ? user.get('ethAddress') : '';

    const defaultKeyParam = useRouter().query.defaultKey;
    const defaultKey = defaultKeyParam ? defaultKeyParam : 'heroes';

    const handleLogin = async () => {
        await authenticate();
    };
    const handleClick = () => {
        if (!isAuthenticated) {
            handleLogin();
        }
    };
    const spawn = new web3.eth.Contract(SPAWN_ABI, SPAWN_CONTRACT);
    const GLAItem = new web3.eth.Contract(ITEM_ABI, ITEM_CONTRACT);
    const GLAToken = new web3.eth.Contract(TOKEN_ABI, TOKEN_CONTRACT);
    const [approveState, setApproveState] = useState(false);
    const [approveStateItem, setApproveStateItem] = useState(false);

    const approveHero = async (heroType) => {
        setLoading(true);
        try {
            if (!approveState) {
                let approveNumber = web3.utils.toWei(BigInt(2 ** 64 - 1).toString(), 'ether');
                await approve(GLAToken, {
                    name: "approve",
                    parameters: [SPAWN_CONTRACT, approveNumber]
                }, user.get('ethAddress'));
                setApproveState(true);
            }
            setLoading(false);

        } catch (e) {
            setLoading(false);
            Swal.fire('Approve error!', '', 'error');
        }
    };

    const handleBuyHero = async (heroType) => {
        setLoading(true);
        if (heroType <= 2) {
            try {
                await spawn.methods
                    .buyNewHero(heroType)
                    .send({ from: user.get('ethAddress') })
                    .then(() => {
                        setLoading(false);
                        Swal.fire({
                            title: 'Done!',
                            html: '<span style="color: white; font-size: 24px">Bought Hero Successfully!</span>',
                            icon: 'success',
                            confirmButtonText: 'Close',
                        });
                    });
            } catch (error) {
                setLoading(false);
                // console.log(error);
                Swal.fire('Bought Hero NOT Successfully!', '', 'error');
            }
        } else {
            setLoading(false);
            // console.log(error);
            Swal.fire('Bought Hero NOT Successfully!', '', 'error');
        }
    }

    useEffect(async () => {
        if (!isWeb3Enabled) {
            enableWeb3();
        }
    }, [web3.currentProvider]);

    const checkApprove = async () => {
        if (isAuthenticated && web3.currentProvider) {
            let allowance = await GLAToken.methods.allowance(ethAddress, SPAWN_CONTRACT).call();
            if (allowance > 0) {
                setApproveState(true);
            } else {
                setApproveState(false);
            }
        }
    }

    useEffect(async () => {
        await checkApprove();
    }, [isAuthenticated])

    useEffect(async () => {
        await checkApprove();
    })

    useEffect(() => {
        Moralis.onAccountsChanged(async function (accounts) {
            await checkApprove();
        });

    }, [])

    const isBox = (pack) => (pack.type === 'box' ? true : false);
    const isHero = (pack) => (pack.type === 'hero' ? true : false);

    return (
        <div className="page-content page-starter-pack">
            <Container>
                <Tab.Container id="left-tabs-example" defaultActiveKey={defaultKey}>
                    <div className="menu-icon-child">
                        <Nav variant="pills" className="menu-child">
                            <Nav.Item className="menu-child-item">
                                <Nav.Link
                                    eventKey="heroes"
                                    className="menu-child-link"
                                >
                                    Heroes
                                </Nav.Link>
                            </Nav.Item>
                            <Nav.Item className="menu-child-item">
                                <Nav.Link
                                    eventKey="item"
                                    className="menu-child-link"
                                >
                                    Items
                                </Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </div>

                    <Tab.Content>
                        <Tab.Pane eventKey="heroes">
                            <Row className="justify-content-center">
                                {packData.map((pack, index) => (
                                    <Col key={index} md={6} lg={3}>
                                        <div className="card-shop">
                                            <div className="card-shop-ava">
                                                <div className="card-shop-about">
                                                    <Image
                                                        src="/assets/images/pages/store/more-infor.png"
                                                        width={58}
                                                        height={58}
                                                        alt="question"
                                                    />
                                                </div>
                                                <div className="card-shop-bg">
                                                    <Image
                                                        src="/assets/images/pages/store/base.png"
                                                        width={372}
                                                        height={142}
                                                        alt="base"
                                                    />
                                                </div>
                                                <div className="card-shop-anim">
                                                    <div className="card-shop-img">
                                                        <Image
                                                            src={pack.image}
                                                            width={200}
                                                            height={200}
                                                            alt="hero"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-shop-meta">
                                                <div className="card-shop-name">
                                                    {pack.name}
                                                </div>
                                                <div className="card-shop-infor">
                                                    <div className="card-shop-prices">
                                                        <div className="card-shop-row">
                                                            <div className="card-shop-label">
                                                                Unit Price
                                                            </div>
                                                            {/* <div className="card-shop-value">
                                                                {pack.price}
                                                            </div> */}
                                                            <div className="card-shop-value card-price">
                                                                <div className="image-price">
                                                                    <Image
                                                                        src={`/assets/images/logo/gla-coin.png`}
                                                                        alt="gla-coin.svg"
                                                                        width={16}
                                                                        height={16}
                                                                    />
                                                                </div>
                                                                <p>{pack.price}</p>
                                                            </div>
                                                        </div>

                                                        {isHero(pack) ? (
                                                            <>
                                                                <div className="card-shop-row">
                                                                    <div className="card-shop-label">
                                                                        Level
                                                                    </div>
                                                                    <div className="card-shop-value">
                                                                        {pack.level}
                                                                    </div>
                                                                </div>
                                                                <div className="card-shop-row">
                                                                    <div className="card-shop-label">
                                                                        Class
                                                                    </div>
                                                                    <div className="card-shop-value">
                                                                        {pack.class}
                                                                    </div>
                                                                </div>
                                                            </>
                                                        ) : (
                                                            ''
                                                        )}
                                                        {isBox(pack) ? (
                                                            <div className="card-shop-row">
                                                                <div className="card-shop-label">
                                                                    Amount
                                                                </div>
                                                                <div className="card-shop-value w-25">
                                                                    <Form.Control
                                                                        size="sm"
                                                                        type="number"
                                                                        defaultValue="1"
                                                                    />
                                                                </div>
                                                            </div>
                                                        ) : (
                                                            ''
                                                        )}
                                                    </div>
                                                </div>
                                                <div className="card-shop-action">
                                                    {!isAuthenticated ? (
                                                        <button
                                                            className="card-shop-button d-block button-connect"
                                                            onClick={
                                                                handleClick
                                                            }
                                                        >
                                                            Connect
                                                        </button>
                                                    ) : null}
                                                    {(!approveState && isAuthenticated) ? (
                                                        <button
                                                            className="card-shop-button d-block button-connect"
                                                            onClick={() =>
                                                                approveHero(
                                                                    pack.hero
                                                                )
                                                            }
                                                        >
                                                            Approve
                                                        </button>
                                                    ) : null}
                                                    {(approveState && isAuthenticated) ? (<button
                                                        className="card-shop-button d-block button-connect"
                                                        onClick={() =>
                                                            handleBuyHero(
                                                                pack.hero
                                                            )
                                                        }
                                                    >
                                                        Buy
                                                    </button>) : null}
                                                    <a
                                                        href="https://pancakeswap.finance/swap?inputCurrency=BNB&outputCurrency=0x292Bb969737372E48D97C78c19B6a40347C33B45"
                                                        target="_blank"
                                                        rel="noreferrer"
                                                        className="card-shop-button d-block button-buy"
                                                    >
                                                        Buy GLA
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </Col>
                                ))}
                            </Row>
                        </Tab.Pane>
                        <Tab.Pane eventKey="item">
                            <Row className="justify-content-center">
                                {packItemData.map((item, index) => {
                                    return (
                                        <ItemDetail
                                            key={index}
                                            item={item}
                                            web3={web3}
                                            isAuthenticated={isAuthenticated}
                                            handleClick={handleClick}
                                            GLAItem={GLAItem}
                                            GLAToken={GLAToken}
                                            ethAddress={ethAddress}
                                            approveStateItem={approveStateItem}
                                            setApproveStateItem={setApproveStateItem}
                                            Moralis={Moralis}
                                        />
                                    );
                                })}
                            </Row>
                        </Tab.Pane>
                    </Tab.Content>
                </Tab.Container>

                <Row className="d-none">
                    {packData.map((pack, index) => (
                        <Col
                            key={index}
                            sm={12}
                            md={6}
                            lg={4}
                            className="mb-4 text-center"
                        >
                            <div className="card-egg card">
                                <div className="card-header">
                                    {getImagePack(pack)}
                                </div>
                                <div className="card-body">
                                    <div className="dark fire">
                                        <h3
                                            className="Blazing"
                                            contentEditable="true"
                                            suppressContentEditableWarning={
                                                true
                                            }
                                        >
                                            {pack.name}
                                        </h3>
                                    </div>
                                    {/* <h3>{pack.name}</h3> */}

                                    <PaymentMethodNormalIcon
                                        title="Price"
                                        value={pack.price}
                                        icon="/assets/img/icon_ada.png"
                                    ></PaymentMethodNormalIcon>
                                    {showPayment(pack)}
                                </div>
                                <div className="card-footer">
                                    {(!isAuthenticated) ? (
                                        <button
                                            type="button"
                                            className="btn btn-new btn-primary btn-block btn-connect"
                                            onClick={handleClick}
                                        >
                                            Connect
                                        </button>
                                    ) : null}
                                    {(!approveState) ? (
                                        <button
                                            type="button"
                                            className="btn btn-new btn-primary btn-block btn-connect"
                                            onClick={() => approveHero(pack.hero)}
                                        >
                                            Approve
                                        </button>
                                    ) : (<button
                                        type="button"
                                        className="btn btn-new btn-primary btn-block btn-connect"
                                        onClick={() => handleBuyHero(pack.hero)}
                                    >
                                        Buy
                                    </button>)
                                    }
                                    <div className="mt-2">
                                        <a
                                            href="https://pancakeswap.finance/swap?inputCurrency=BNB&outputCurrency=0x292Bb969737372E48D97C78c19B6a40347C33B45"
                                            target="_blank"
                                            rel="noreferrer"
                                            className="small"
                                        >
                                            <i className="fas fa-external-link-alt"></i>
                                            Buy GLA
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </Col>
                    ))}
                </Row>
            </Container>
        </div>
    );
}

const ItemDetail = ({
    web3,
    item,
    isAuthenticated,
    handleClick,
    GLAItem,
    GLAToken,
    ethAddress,
    approveStateItem,
    setApproveStateItem,
    Moralis
}) => {
    const [amountItem, setAmountItem] = useState(item.defaultAmount);
    useEffect(async () => {
        await checkApprove();
    })

    useEffect(async () => {
        await checkApprove();
    }, [isAuthenticated])

    const checkApprove = async () => {
        if (isAuthenticated && web3.currentProvider) {
            let allowance = await GLAToken.methods.allowance(ethAddress, ITEM_CONTRACT).call();
            if (allowance > 0) {
                setApproveStateItem(true);
            } else {
                setApproveStateItem(false);
            }
        }
    }

    const handleChangeAmountItem = (e, type) => {
        let value = e.target.value;
        setAmountItem(value);
    };
    const handleApprove = async (itemType, amount) => {
        setLoading(true);
        try {
            if (!approveStateItem) {
                let approveNumber = web3.utils.toWei(BigInt(2 ** 64 - 1).toString(), 'ether');
                await approve(GLAToken, {
                    name: "approve",
                    parameters: [ITEM_CONTRACT, approveNumber]
                }, ethAddress)
            }
            setApproveStateItem(true);
            setLoading(false);
        } catch (e) {
            setApproveStateItem(false);
            setLoading(false);
            Swal.fire('Approve error!', '', 'error');
        }
    };
    const handleBuy = async (itemType, amount) => {
        setLoading(true);
        try {
            await GLAItem.methods
                .buyItem(itemType, amount)
                .send({ from: ethAddress })
                .then(() => {
                    setLoading(false);
                    Swal.fire({
                        title: 'Done!',
                        html: '<span style="color: white; font-size: 24px">Bought Item Successfully!</span>',
                        icon: 'success',
                        confirmButtonText: 'Close',
                    });
                });
        } catch (error) {
            setLoading(false);
            // console.log(error);
            Swal.fire('Bought Item NOT Successfully !', '', 'error');
        }
    }
    useEffect(() => {
        Moralis.onAccountsChanged(async function (accounts) {
            await checkApprove();
        });

    }, [])
    return (
        <Col md={6} lg={3}>
            <div className="card-shop">
                <div className="card-shop-ava">
                    <div className="card-shop-about">
                        <Image
                            src="/assets/images/pages/store/more-infor.png"
                            width={58}
                            height={58}
                            alt="question"
                        />
                    </div>
                    <div className="card-shop-bg">
                        <Image
                            src="/assets/images/pages/store/base.png"
                            width={372}
                            height={142}
                            alt="base"
                        />
                    </div>
                    <div className="card-shop-anim">
                        <div className="card-shop-img">
                            <Image src={item.image} width={200} height={200} alt="hero" />
                        </div>
                    </div>
                </div>
                <div className="card-shop-meta">
                    <div className="card-shop-name">{item.name}</div>
                    <div className="card-shop-infor">
                        <div className="card-shop-prices">
                            <div className="card-shop-row">
                                <div className="card-shop-label">Unit Price</div>
                                <div className="card-shop-value card-price">
                                    <div className="image-price">
                                        <Image
                                            src={`/assets/images/logo/gla-coin.png`}
                                            alt="gla-coin.svg"
                                            width={16}
                                            height={16}
                                        />
                                    </div>
                                    <p>{item.price}</p>
                                </div>
                                {/* <div className="card-shop-value">
                                    {item.price}
                                </div> */}
                            </div>
                            <div className="card-shop-row">
                                <div className="card-shop-label">Amount</div>
                                <div className="card-shop-value w-25">
                                    <Form.Control
                                        size="sm"
                                        type="number"
                                        step={item.name === "Gem" ? "100" : "1"}
                                        value={amountItem}
                                        onChange={(value) =>
                                            handleChangeAmountItem(
                                                value,
                                                item.name
                                            )
                                        }
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card-shop-action">
                        {!isAuthenticated ? (
                            <button
                                className="card-shop-button d-block button-connect"
                                onClick={handleClick}
                            >
                                Connect
                            </button>
                        ) : null}
                        {((isAuthenticated && !approveStateItem) ?
                            (<button
                                className="card-shop-button d-block button-connect"
                                onClick={() =>
                                    handleApprove(item.name, amountItem)
                                }
                            >
                                Approve
                            </button>) : null
                        )}
                        {((isAuthenticated && approveStateItem) ?
                            (<button
                                className="card-shop-button d-block button-connect"
                                onClick={() =>
                                    handleBuy(item.name, amountItem)
                                }
                            >
                                Buy
                            </button>) : null
                        )}
                        <a
                            href="https://pancakeswap.finance/swap?inputCurrency=BNB&outputCurrency=0x292Bb969737372E48D97C78c19B6a40347C33B45"
                            target="_blank"
                            rel="noreferrer"
                            className="card-shop-button d-block button-buy"
                        >
                            Buy GLA
                        </a>
                    </div>
                </div>
            </div>
        </Col>
    );
};
